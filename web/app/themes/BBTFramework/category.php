<?php
/**
 * Template Name: Haven Blog
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a page ('page' post_type) unless another page template overrules this one.
 * @link http://codex.wordpress.org/Pages
 *
 * @package WooFramework
 * @subpackage Template
 */

get_header();
?>
       
    <!-- #content Starts -->
    <?php woo_content_before(); ?>
    <div id="content" class="col-full">
    
        <div id="main-sidebar-container">    

            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main">
                <div class="blog-header"><p>Haven news and blog</p></div>
                <div class="haven-container-a">
                    <div class="row">
                        <div class="post-search col-12"><?php get_search_form(); ?></div>
                    </div>
                </div>
                <div class="haven-container-a post-grid">
                    <div class="row">
                    <?php
                    woo_loop_before();
                    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                    $loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 5, 'paged' => $paged ) );
                    if ( $loop->have_posts() ) : 
                        $i = 0;
                        while ( $loop->have_posts() ) : $loop->the_post();
                        if ( $i == 0 ) : ?>
                        <div class="col-md-12">
                        <?php endif;
                        if ( $i != 0 ) : ?>
                        <div class="col-md-6">
                        <?php endif; ?>
                            <div class="post-image"><a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail();?></a></div>
                            <div class="post-content-wrapper">
                                <div class="row">
                                    <div class="col-8"><div class="post-date"><?php the_date( 'j F Y' ); ?></div><div class="post-category"><?php the_category(); ?></div></div>
                                    <div class="col-4">
                                        <div class="post-social-wrap">
                                            <i class="material-icons share-trig">share</i>
                                            <div class="post-social">Share: <div class="button st-custom-button" data-network="email"><i class="material-icons">email</i></div><div class="button st-custom-button" data-network="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></div></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                <div class="post-content"><?php echo get_excerpt(); ?></div>
                            </div>
                        </div>
                    <?php $i++;
                    endwhile;?>
                    <div class="post-pagination col-md-12">
                        <?php 
                        $big = 999999999; // need an unlikely integer
 
                        echo paginate_links( array(
                            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                            'format' => '?paged=%#%',
                            'current' => max( 1, get_query_var('paged') ),
                            'total' => $loop->max_num_pages
                        ) );
                        ?>
                    </div>
                    <?php endif; ?> 
                    <?php woo_loop_after(); ?> 
                    </div>
                </div>
            </section><!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>

        </div><!-- /#main-sidebar-container -->         

        <?php get_sidebar( 'alt' ); ?>

    </div><!-- /#content -->
    <?php woo_content_after(); ?>

<?php get_footer(); ?>