<?php $app_form_id = 3854; //application form mortgages

if( $post->ID != $app_form_id ) : ?>
    <div class="col-md-3 hello">
        <div class="benefit-img <?php echo $post->ID; ?>"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a></div>
        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        <div class="benefits-text"><p><?php the_field('page_summary'); ?></p></div>
        <div class="button-white-black">
            <a href="<?php the_permalink(); ?>">Read more</a>
        </div> 
    </div>
<?php else : 
    $image = get_field('we_thumbnail', $app_form_id); ?>

    <div class="col-md-3 hello2">
        <div class="benefit-img <?php echo $post->ID; ?>">
            <a href="<?php the_permalink(); ?>">
                <?php if( !empty($image) ): ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php endif; ?>
            </a>
        </div>
        <h4>
            <a href="<?php the_permalink(); ?>">
                <?php if(get_field('we_title', $app_form_id)) : ?>
                    <?php the_field('we_title', $app_form_id); ?>
                <?php endif; ?>
            </a>
        </h4>
        <div class="benefits-text"><p>
            <?php if(get_field('we_summary', $app_form_id)) : ?>
                <?php the_field('we_summary', $app_form_id); ?>
            <?php endif; ?></p>
        </div>
        <div class="button-white-black">
            <a href="<?php the_permalink(); ?>">Read more</a>
        </div> 
    </div>
<?php endif; ?>