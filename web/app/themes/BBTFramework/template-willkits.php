<?php
/**
 * Template Name: Will Kits
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a page ('page' post_type) unless another page template overrules this one.
 * @link http://codex.wordpress.org/Pages
 *
 * @package WooFramework
 * @subpackage Template
 */

 get_header();
?>

    <!-- #content Starts -->
    <?php woo_content_before(); ?>
    <div class="content">
        <section id="main">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; endif;
            wp_reset_postdata(); ?>
        </section><!-- /#main -->
    </div>

    <div style="display:none">
        <div id="will-kit-form-WSC01" class="will-kit-form">
            <?php gravity_form( 'Free Will Kit Form WSC01', false, true, false, null, true, 1, true ); ?>
        </div>
        <div id="will-kit-form-WSNC01" class="will-kit-form">
            <?php gravity_form( 'Free Will Kit Form WSNC01', false, true, false, null, true, 1, true ); ?>
        </div>
        <div id="will-kit-form-WSS01" class="will-kit-form">
            <?php gravity_form( 'Free Will Kit Form WSS01', false, true, false, null, true, 1, true ); ?>
        </div>
        <div id="will-kit-form-WSSC01" class="will-kit-form">
            <?php gravity_form( 'Free Will Kit Form WSSC01', false, true, false, null, true, 1, true ); ?>
        </div>
    </div>

    <?php woo_content_after(); ?>
<?php get_footer(); ?>