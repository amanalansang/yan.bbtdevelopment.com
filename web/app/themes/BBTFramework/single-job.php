<?php get_header(); ?>
<script type="text/javascript" href=""></script>
<div class="content">
    <div class="haven-breadcrumbs">
        <div class="haven-breadcrumbs-inner">
            <?php   
                if ( function_exists( 'yoast_breadcrumb' ) ) {
                    yoast_breadcrumb();
                } 
            ?>
        </div>
    </div>
      <div class="post_white_section">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="haven-container-a">
          			<div class="row post-title">
                        <div class="col-12"><?php the_title(); ?></div>
                    </div>
                    <div class="row post-date-category">
                        <div class="col-md-7">
                            <div class="job-metas">
                                <span class="job-location"><?php the_field('location');?></span>
                                <span class="job-type"><?php the_field('work_type');?></span>
                                <?php
                                $now    = time();
                                $date   = get_field('date_closes');
                                if($date) {
                                    $date   = strtotime($date);
                                    $class  = ($date > $now) ? 'job-available' : 'job-expired';
                                    
                                    echo '<span class="job-status '.$class.'">Closes: '.date("m-d-Y", $date).'</span>';
                                }
                                ?>                                       
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="post-social"><span><i class="material-icons">share</i>Share this article:</span><div class="button st-custom-button" data-network="email"><i class="material-icons">email</i></div><div class="button st-custom-button" data-network="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></div></div>
                        </div>
                    </div>
                    <div class="col-12">
              			<div class="row career-sections">
                            <div class="career-section career-summary">
                                <h2 class="section-title"><?php the_field('career_summary_title');?></h2>
                                <div class="section-content"><?php the_content(); ?></div>
                            </div>

                            <div class="career-section career-role">
                                <h2 class="section-subtitle"><?php the_field('role_title');?></h2>
                                <div class="section-content"><?php the_field('role_description');?></div>
                            </div>

                             <div class="career-section career-requirement">
                                <h2 class="section-subtitle"><?php the_field('requirement_title');?></h2>
                                <div class="section-content"><?php the_field('requirement_content');?></div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="row post-nav">
                        <div class="col-12">
                            <div class="alignleft"><?php next_post_link(' %link') ?></div>
                            <div class="alignright"><?php previous_post_link('%link') ?></div>
                        </div>
                    </div> -->
                    <div class="clearfix"></div>
        </div>
        <div class="career-contact-email">
            <div class="career-section career-contacts haven-container">
                <div class="section-content"><?php the_field('contact');?></div>
            </div>                
        </div>        
  		<?php endwhile;?>
  		<?php endif; ?>
      </div>
    </div>

<?php get_footer(); ?>