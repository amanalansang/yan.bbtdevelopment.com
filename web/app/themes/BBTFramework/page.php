
<?php
 get_header();
?>
    <!-- #content Starts -->
  <?php woo_content_before(); ?>
 <div class="content">
    <section id="main">
        <?php /* if(is_front_page()) : ?>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory') ?>/css/home.css">
        <div class="row vidImgBanner top-home">
            <div class="col-lg-12 noPadd">
                <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="10000">
                    <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                  </ol>

                  <div class="carousel-inner">
                    <div class="item active">
                        <a class="ctaButton" href="https://www.facebook.com/notes/haven-financial-advisers/be-in-to-win-with-havens-tenth-birthday/10155930284348106/" target="_target">
                            <video class="forDesktop" id="myvideo" width="100%" height="560" playsinline autoplay muted loop>
                                <source src="<?php bloginfo('stylesheet_directory'); ?>/images/1200x520_GiftCard1-compressed (1).mp4" type="video/mp4" />
                                Your browser does not support the video tag.
                            </video>
                            <script> document.getElementById('myvideo').play(); </script>
                            <img class="forIpad" src="<?php bloginfo('stylesheet_directory'); ?>/images/10th-anniv-banner-ipad.gif" />
                            <img class="forMobile" src="<?php bloginfo('stylesheet_directory'); ?>/images/10th-anniv-banner-mobile.gif" />
                        </a>
                    </div>
                    <div class="item">
                        <div class="bg">
                            <img class="forMobileImg" src="<?php bloginfo('stylesheet_directory'); ?>/images/mobile-trans.png" />
                            <div class="top-container-text" style="position: absolute; top: 0; left:15%">
                                <div class="wpb_wrapper">
                                    <h1 style="text-align: left;">Set yourself up for<br> a brighter future.</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <?php endif;*/ ?>
        <?php get_template_part( 'loop', 'archive' ); ?>
    </section><!-- /#main -->
    <?php woo_content_after(); ?>
    <?php $posts = get_field('page_display');
    if( $posts ): ?>
    <div class="other-services">
        <div class="haven-container">
            <h2>We think you might also like</h2>
        </div>
        <div class="haven-container">
            <div class="row">
            <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                <?php setup_postdata($post); ?>
                
                <?php get_template_part( 'like' ); ?>
            <?php endforeach; ?>
            </div>
        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
        </div>
    </div>
    <?php endif; ?> 
</div>
  <style>
  @media screen and (max-width: 450px) {
      .contact-form .gform_footer {
        padding:0px !important;
        bottom:-5px !important;
      }
      .contact-form input#gform_submit_button_5 {
          width: 100% !important;
          float: none !important;
          position: relative !important;
          top: 0 !important;
      }
    }
  </style>
<?php get_footer(); ?>