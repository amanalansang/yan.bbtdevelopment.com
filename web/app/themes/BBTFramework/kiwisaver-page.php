<?php
/**
 * Page Content Template
 *
 * This template is the default page content template. It is used to display the content of the
 * `page.php` template file, contextually, as well as in archive lists or search results.
 *
 * @package WooFramework
 * @subpackage Template
 */

/**
 * Settings for this template file.
 *
 * This is where the specify the HTML tags for the title.
 * These options can be filtered via a child theme.
 *
 * @link http://codex.wordpress.org/Plugin_API#Filters
 */
 global $woo_options;

 $heading_tag = 'h1';
 if ( is_front_page() ) { $heading_tag = 'h2'; }
 $title_before = '<' . $heading_tag . ' class="title entry-title">';
 $title_after = '</' . $heading_tag . '>';

 $page_link_args = apply_filters( 'woothemes_pagelinks_args', array( 'before' => '<div class="page-link">' . __( 'Pages:', 'woothemes' ), 'after' => '</div>' ) );

 woo_page_before();
?>
<article <?php post_class(); ?>>
<?php
	woo_page_inside_before();
?>

<script>
jQuery(document).ready(function($) {

	$(window).on('load', function(){
		//$('#logo img').attr('src', '/app/uploads/2017/08/logo-blue.png');
	});

});
</script>

<style>
.top-container{
	margin-top: 0px;
	background-color: #00bce7;
	padding-top:0px;
	min-height: 159px;
}

.top-container-inner{
	padding-top: 44px;
}

.top-container h1{
	font-size: 20px;
	line-height: 22px;
}

.top-container h2{
	font-size: 30px;
	line-height: 33px;
	margin-bottom: 0px !important;
    padding-bottom: 27px !important;
    max-width: 100%;
}

.top-container-title img{
	margin-right: 14px;
}

.top-container-title{
	margin-bottom: 24px;
}

</style>

	<header>
		<?php //the_title( $title_before, $title_after ); ?>
	</header>

	<div class="haven-breadcrumbs">
		<div class="haven-breadcrumbs-inner">
			<?php	
				if ( function_exists( 'yoast_breadcrumb' ) ) {
					yoast_breadcrumb();
				} 
			?>
		</div>
	</div>

	<section class="entry">
		<div class="top-container" id="top-kiwisaver">
			<div class="top-container-inner">
				<div class="top-container-title">
                    <?php 
                        $current = $post->ID;
                        $parent = $post->post_parent;
                        $get_grandparent = get_post($parent);
                        $grandparent = $get_grandparent->post_parent;
                        if(get_the_title($parent) == 'Mortgages' || get_the_title($grandparent) == 'Mortgages') {
                            echo '<img src="/app/themes/BBTFramework/images/mortgage-icon.png">';
                        } else if(get_the_title($parent) == 'KiwiSaver' || get_the_title($grandparent) == 'KiwiSaver') {
                            echo '<img src="/app/uploads/2017/08/arrow-ks.png">';
                        } else if(get_the_title($parent) == 'Insurance' || get_the_title($grandparent) == 'Insurance') {
                            echo '<img src="/app/themes/BBTFramework/images/insurance-icon.png">';
                        } else if(get_the_title($parent) == 'Accounting' || get_the_title($grandparent) == 'Accounting') {
                            echo '<img src="/app/uploads/2017/11/accounting-icon-1.png">';
                        }
                    ?>
                    <h1>
                    <?php
                        if($grandparent != '') {
                            echo get_the_title($grandparent);     
                        } else {
                            echo get_the_title($parent); 
                        }
                    ?>
                    </h1>
                </div>
                <h2>
                    <?php
                    if(get_field('p_custom_title')) {
                        $title = get_field('p_custom_title');
                    } else {
                        $title = get_the_title();
                    }

                    echo $title; ?>
                </h2>
			</div>
		</div>

	    <?php
	    	the_content( __( 'Continue Reading &rarr;', 'woothemes' ) );
	    	wp_link_pages( $page_link_args );
	    ?>
	</section><!-- /.entry -->
	<div class="fix"></div>
<?php
	woo_page_inside_after();
?>
</article><!-- /.post -->
<?php
	woo_page_after();
	$comm = get_option( 'woo_comments' );
	if ( ( $comm == 'page' || $comm == 'both' ) && is_page() ) { comments_template(); }
?>