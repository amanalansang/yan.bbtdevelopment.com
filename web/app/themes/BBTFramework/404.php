
<?php
 get_header();
?>
 <div class="content">
    <section id="main" class="haven-container error-main-wrap">
        <div class="row">
            <div class="col-md-4">
                <div class="error-main">404</div>
                <div class="error-title">We can’t seem to find the page you requested.</div>
                <div class="error-content">We can’t seem to find the page you’re looking for.<br />
I’m sorry, it appears that you have taken the wrong turn. Head back the way you came and we’ll meet you there.</div>
                <div class="error-btns">
                    <div class="button-white"><a href="/">Back to homepage</a></div>
                    <div class="button-blue"><a href="/contact-us">Contact us</a></div>
                </div>
            </div>
            <div class="col-md-8"><img src="/app/themes/BBTFramework/images/error.png"></div>
        </div>
    </section>
</div>
<?php get_footer(); ?>