
<?php
/**
 * Archive Template
 *
 * The archive template is a placeholder for archives that don't have a template file.
 * Ideally, all archives would be handled by a more appropriate template according to the
 * current page context (for example, `tag.php` for a `post_tag` archive).
 *
 * @package WooFramework
 * @subpackage Template
 */

 global $woo_options;
 get_header();
?>
    <!-- #content Starts -->
  <?php woo_content_before(); ?>
 <div class="content">
      <div class="post_white_section">
        <div class="container">
          <div class="row content_row_blogs">
          <div class="col-md-9" id="left_row">
                <section id="main">

                  <?php get_template_part( 'loop', 'archive' ); ?>

            </section><!-- /#main -->

                </div>
          <div class="col-md-3" id="right_row">

                <h1 class="title_categories">Categories</h1>
                <?php the_widget('WP_Widget_Categories' ); ?>

          </div>
          </div>
        </div>
      </div>
    </div>
    <?php woo_content_after(); ?>

<?php get_footer(); ?>