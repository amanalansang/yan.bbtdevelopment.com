<?php
/**
 * Header Template
 *
 * Here we setup all logic and XHTML that is required for the header section of all screens.
 *
 * @package WooFramework
 * @subpackage Template
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>" />
<?php if( is_page( array('accounting-adwords', 'accounting-adwords-thank-you') )  ) : ?>
<meta name="robots" content="noindex">
<?php endif; ?>
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="/app/themes/BBTFramework/js/jquery.bxslider.js"></script>
<script src="/app/themes/BBTFramework/js/jquery.bxslider.min.js"></script>
<link href="/app/themes/BBTFramework/js/jquery.bxslider.css" rel="stylesheet" /> -->

<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5a266110689cdf0012ad4be6&product=sop' async='async'></script>

<!-- <script>
    jQuery(document).ready(function($) {
        //to be added later     
        // $(window).on('load resize', function() {
        //  $('.why-financial-adviser-inner > .vc_column-inner').equalizeHeights();

        // });

        $.fn.equalizeHeights = function(){
            return this.height( Math.max.apply(this, $(this).map(function(i,e){ return $(e).height() }).get() ) )
        }
    });
</script> -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5R4TFNV');</script>
<!-- End Google Tag Manager -->
<title><?php woo_title(); ?></title>
<?php woo_meta(); ?>
<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>" />
<?php wp_head(); ?>
<?php woo_head(); ?>

</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5R4TFNV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=625516417635895&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php woo_top(); ?>
<div id="black-screen"></div>
<a id="gf_11" style="height: 0px; visibility: hidden;"></a>
<div id="wrapper">

	<div id="inner-wrapper">
    <div class="header-wrap">
    	<?php woo_header_before(); ?>
    	<header id="header" class="col-full">
    		<?php woo_header_inside(); ?>
    	</header>       
        <!-- <?php if(is_page('our-story')) { ?>
            <div class="header-about">
                    <?php wp_nav_menu( array(
                        'menu' => 'About Menu',
                    ) ); ?>
            </div>
        <?php } ?> -->
    </div>
	<?php woo_header_after(); ?>
    <header id="mobile_header">
        <h3>
            <!--<a href="/"><img src="/app/themes/BBTFramework/images/logo-white.png" alt=""></a>-->
			<?php 
            if (is_singular('job') || is_page('application-form') || is_page('thank-you')) {
                echo "<a href='/'><img src='/app/uploads/2018/03/logo-blue.png' alt='Haven Accountants and Financial Advisers'></a>";
            } else {
                echo "<a href='/'><img src='/app/themes/BBTFramework/images/logo-white.png' alt='Haven Accountants and Financial Advisers'></a>";
            }
            ?>
            <a class="cd-dropdown-trigger" href="#0"></a>
        </h3>
        <div class="cd-dropdown-wrapper">
            <?php html5blank_mega(); ?> 
        </div>
    </header>
    <div class="search__header">
        <div class="haven-container">
            <i class="fa fa-search"></i><?php dynamic_sidebar( 'search-header' ); ?><span class="close"></span>
        </div>
    </div> 