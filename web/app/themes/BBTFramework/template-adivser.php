
<?php
/*
template name: Advisers
*/
 get_header();
?>
    <!-- #content Starts -->
  <?php woo_content_before(); ?>
 <div class="content adviser-template">
    <section id="main">
          <?php get_template_part( 'loop', 'archive' ); ?>
    </section><!-- /#main -->
    <?php woo_content_after(); ?>
    <?php $posts = get_field('page_display');
    if( $posts ): ?>
    <div class="other-services">
        <div class="haven-container">
            <h2>We think you might also like</h2>
        </div>
        <div class="haven-container">
            <div class="row">
            <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                <?php setup_postdata($post); ?>
                <div class="col-md-3">
                    <div class="benefit-img"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('full'); ?></a></div>
                    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                    <div class="benefits-text"><p><?php the_field('page_summary'); ?></p></div>
                    <div class="button-white-black">
                        <a href="<?php the_permalink(); ?>">Read more</a>
                    </div> 
                </div>
            <?php endforeach; ?>
            </div>
        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
        </div>
    </div>
    <?php endif; ?> 
</div>
<?php get_footer(); ?>