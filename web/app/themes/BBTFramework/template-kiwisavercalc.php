<?php
/**
 * Template Name: KiwisaverCalculator
 *
 */
 get_header();
?>
    <!-- #content Starts -->
  <?php woo_content_before(); ?>
 <div class="content">
    <section id="main">
        <?php get_template_part( 'loop', 'archive' ); ?>
    </section><!-- /#main -->
    <?php woo_content_after(); ?>
    <?php $posts = get_field('page_display');
    if( $posts ): ?>
    <div class="other-services">
        <div class="haven-container">
            <h2>We think you might also like</h2>
        </div>
        <div class="haven-container">
            <div class="row">
            <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                <?php setup_postdata($post); ?>
                
                <?php get_template_part( 'like' ); ?>
            <?php endforeach; ?>
            </div>
        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
        </div>
    </div>
    <?php endif; ?> 
</div>
<?php get_footer(); ?>