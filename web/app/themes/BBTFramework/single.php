<?php get_header(); ?>
    <div class="content mb-5">
        <div class="haven-breadcrumbs">
            <div class="haven-breadcrumbs-inner">
                <?php
                if (function_exists('yoast_breadcrumb')) {
                    yoast_breadcrumb();
                }
                ?>
            </div>
        </div>
        <div class="post_white_section">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-lg-8 col-md-12 text">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div class="row post-title no-gutters">
                                <div class="col-12"><?php the_title(); ?></div>
                            </div>
                            <div class="row post-date-category no-gutters">
                                <div class="col-md-6">
                                    <div class="post-date"><?php the_time('j F Y'); ?></div>
                                    <div class="post-category"><?php the_category(); ?></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="post-social"><span><i class="material-icons">share</i>Share this article:</span>
                                        <div class="button st-custom-button" data-network="email"><i
                                                    class="material-icons">email</i></div>
                                        <div class="button st-custom-button" data-network="facebook"><i
                                                    class="fa fa-facebook" aria-hidden="true"></i></div>
                                    </div>
                                </div>
                            </div>

                            <?php $thumb_id = get_post_thumbnail_id();
                            $thumb_url = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
                            $styles = $thumb_url[0] != "" && $thumb_url[0] != "http://haven.local/wp/wp-includes/images/media/default.png" ?
                            "background: url(" . $thumb_url[0] . ") no-repeat; background-size: cover" : "background: url(/app/themes/BBTFramework/images/logo-white.png) center center no-repeat #00bce7; background-size: 50%" ?>
                            <div class="row post-feat-image mb-4 no-gutters">
                                <div class="col-md-12 image" style="<?php echo $styles; ?>">
                                    <img src="<?php echo $thumb_url[0] ?>" style="visibility: hidden;"/>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-4"></div>
                </div>
            </div>
            <div class="container mobile">
                <div class="row d-flex align-items-top no-gutters">
                    <div class="col-lg-8 col-md-12">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div class="row post-content text no-gutters">
                                <div class="col-12">
                                    <?php the_content() ?>
                                </div>
                            </div>

                            <?php if(get_field('spf_title', 'options') || get_field('spf_subtitle', 'options') || the_field('spf_title') || the_field('spf_subtitle')) : ?>
                                <div class="row contact no-gutters">
                                    <div class="col-12 post-contact-box">
                                        <div class="inner">
                                            <h3><?php echo get_field('spf_title') ? the_field('spf_title') : get_field('spf_title', 'options')?></h3>
                                            <p><?php echo get_field('spf_subtitle') ? the_field('spf_subtitle') : get_field('spf_subtitle', 'options')?></p>

                                                <a href="<?php echo get_field('spf_booking_link_url') ? the_field('spf_booking_link_url') : get_field('spf_booking_link_url', 'options') ?>" class="button">
                                                    <?php echo get_field('spf_booking_link_text') ? the_field('spf_booking_link_text') : get_field('spf_booking_link_text', 'options'); ?>

                                                </a>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="row post-content share no-gutters">
                                <div class="col-12 post-social"><span><i class="material-icons">share</i>Share this article:</span>
                                    <div class="button st-custom-button" data-network="email"><i
                                                class="material-icons">email</i></div>
                                    <div class="button st-custom-button" data-network="facebook"><i
                                                class="fa fa-facebook" aria-hidden="true"></i></div>
                                </div>
                            </div>
                            <div class="row post-nav no-gutters">
                                <div class="col-12 p-0">
                                    <div class="alignleft"><?php next_post_link(' %link') ?></div>
                                    <div class="alignright"><?php previous_post_link('%link') ?></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>

                    <div class="col-md-12 mobile d-block d-lg-none d-xs-none pl-3 pr-3">
                        <div class="row">
                            <div class="col-12">
                                <div class="newsletter">
                                    <?php $title = get_field('sbt_title', 'options'); ?>
                                    <div class="title"> <?php echo $title != "" ? $title : "Stay up to date"; ?></div>
                                    <p><?php echo get_field('sbt_subtitle', 'options'); ?></p>
                                    <?php gravity_form( 1, false, false, false, '', false ); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 sidebar mt-4">
                        <?php
                        $orig_post = $post; 
                        global $post;   

                        $postcats = get_the_category( $post->ID );

                        if ($postcats) {    
                            $cat_ids = array(); 
                            foreach($postcats as $individual_cat) $cat_ids[] = $individual_cat->term_id;    
                            $args = array(    
                                'category__in' => $cat_ids,  
                                'post_status' => 'publish',
                                'post__not_in' => array($post->ID), 
                                'posts_per_page' => 3, 
                                'ignore_sticky_posts' => 1   
                            );

                            $my_query = new wp_query( $args );  

                            if($my_query->have_posts()) : ?>
                                <div class="articles">
                                    <div class="title"><span>Related articles</span></div>
                                    <div class="contents blog-gallery">
                                        <?php while ( $my_query->have_posts() ) : $my_query->the_post();
                                            $thumb_id = get_post_thumbnail_id();
                                            $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
                                            $styles = $thumb_url[0] != "" && $thumb_url[0] != "http://haven.local/wp/wp-includes/images/media/default.png" ?
                                                "background-image: url(". $thumb_url[0] .")" : "background: url(/app/themes/BBTFramework/images/logo-white.png) center center no-repeat #00bce7;  
                                                background-size: 60px auto;"; ?>

                                            <div class="item row no-gutters d-flex align-items-top">
                                                <div class="col-lg-3 col-md-12">
                                                    <a href="<?php the_permalink(); ?>">
                                                        <div class="image" style="<?php echo $styles; ?>">
                                                            <div class="post-image"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-lg-9 col-md-12 contents">
                                                    <a href="<?php the_permalink(); ?>">
                                                        <h5><?php the_title(); ?></h5>
                                                    </a>
                                                    <a href="<?php the_permalink(); ?>">
                                                        <p><?php the_time('d.m.Y'); ?></p>
                                                    </a>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                </div>
                            <?php endif;
                        } // end iftags
                        $post = $orig_post; 
                        wp_reset_query(); ?>

                        <div class="newsletter desktop d-none d-lg-block d-xs-block">
                            <?php $title = get_field('sbt_title', 'options'); ?>
                            <div class="title"> <?php echo $title != "" ? $title : "Stay up to date"; ?></div>
                            <p><?php echo get_field('sbt_subtitle', 'options'); ?></p>
                            <?php gravity_form( 1, false, false, false, '', false ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>