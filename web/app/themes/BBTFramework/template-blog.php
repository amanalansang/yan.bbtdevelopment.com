<?php
/**
 * Template Name: Haven Blog
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a page ('page' post_type) unless another page template overrules this one.
 * @link http://codex.wordpress.org/Pages
 *
 * @package WooFramework
 * @subpackage Template
 */

get_header(); ?>

    <!-- #content Starts -->
<?php woo_content_before(); ?>

    <div id="content" class="col-full mb-5">

        <div id="main-sidebar-container">

            <!-- #main Starts -->
            <?php woo_main_before(); ?>

            <section id="main">

                <div class="blog-header"><p>Haven news and blog</p></div>

                <div class="haven-container-a">
                    <div class="row">
                        <div class="post-search col-12"><?php get_search_form(); ?></div>
                    </div>
                </div>

                <?php woo_loop_before(); ?>

                <?php $loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 1) );
                if ( $loop->have_posts() ) : ?>
                    <div class="haven-container-a post-grid">
                        <div class="row">
                            <div class="col-md-12">
                                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                    <div class="post-image"><a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail();?></a></div>
                                    <div class="post-content-wrapper">
                                        <div class="row">
                                            <div class="col-8">
                                                <div class="post-date"><?php the_time( 'j F Y' ); ?></div>
                                                <div class="post-category"><?php the_category(); ?></div>
                                            </div>
                                            <div class="col-4">
                                                <div class="post-social-wrap">
                                                    <i class="material-icons share-trig">share</i>
                                                    <div class="post-social">Share: <div class="button st-custom-button" data-network="email"><i class="material-icons">email</i></div><div class="button st-custom-button" data-network="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></div></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                        <div class="post-content desktop d-none d-md-block"><?php echo excerpt(55); ?></div>
                                        <div class="post-content mobile d-block d-md-none"><?php echo excerpt(16); ?></div>
                                    </div>
                                <?php endwhile;?>
                            </div>
                        </div>
                    </div>
                <?php endif;
                wp_reset_query();
                wp_reset_postdata(); ?>

                <div class="col-md-12 mobile d-block d-lg-none d-xs-none pl-3 pr-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="newsletter">
                                <?php $title = get_field('sbt_title', 'options'); ?>
                                <div class="title"> <?php echo $title != "" ? $title : "Stay up to date"; ?></div>
                                <p><?php echo get_field('sbt_subtitle', 'options'); ?></p>
                                <?php gravity_form( 1, false, false, false, '', false ); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!--POSTS-->
                <?php
                $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                $posts_per_page = 4;
				$offset = ( $paged - 1 ) * $posts_per_page + 1;
                $loopDesktop = new WP_Query( array( 'post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => $posts_per_page, 'paged' => $paged, 'offset' => $offset ) );?>
                <div class="container blog-grid">
                    <div class="row">
                        <?php if ( $loopDesktop->have_posts() ) : ?>
                            <div class="col-lg-8 col-md-12 content_wrapper desktop d-none d-md-block">
                                <div class="row">
                                    <?php while ( $loopDesktop->have_posts() ) : $loopDesktop->the_post();
                                        $thumb_id = get_post_thumbnail_id();
                                        $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
                                        $styles = $thumb_url[0] != "" && $thumb_url[0] != "http://haven.local/wp/wp-includes/images/media/default.png" ?
                                                "background-image: url(". $thumb_url[0] ."); background-size: cover" : "background: url(/app/themes/BBTFramework/images/logo-white.png) center center no-repeat #00bce7; background-size: 132px 33px;"?>
                                        <div class="col-md-6 post-image-wrapper">
                                            <a href="<?php the_permalink(); ?>" style="<?php echo $styles; ?>">
                                                <div class="post-image"></div>
                                            </a>
                                            <div class="post-content-wrapper">
                                                <div class="row post-social">
                                                    <div class="col-10">
                                                        <div class="post-date">
                                                            <?php the_time('j F Y');  ?>
                                                        </div>
                                                        <div class="post-category"><?php the_category(); ?></div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="post-social-wrap">
                                                            <i class="material-icons share-trig">share</i>
                                                            <div class="post-social">Share: <div class="button st-custom-button" data-network="email"><i class="material-icons">email</i></div><div class="button st-custom-button" data-network="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                                <div class="post-content desktop"><?php echo excerpt(35) ?></div>
                                                <div class="post-content ipad"><?php echo excerpt(30) ?></div>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 pagination_wrapper post-pagination1">
                                        <div class="pagination_links">
                                            <?php
                                            $big = 999999999; // need an unlikely integer
                                            $max = ceil($loopDesktop->max_num_pages / $posts_per_page);
                                            echo paginate_links( array(
                                                //'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                                'base' => preg_replace('/\?.*/', '/', get_pagenum_link()) . '%_%',
                                                'format' => '?paged=%#%',
                                                'current' => max( 1, $paged ),
                                                'total' => $max
                                            ) );
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif;
                        wp_reset_query();
                        wp_reset_postdata(); ?>

                        <?php 

                        if ( $loopDesktop->have_posts() ) : ?>
                            <div class="col-lg-8 col-md-12 content_wrapper mobile d-block d-md-none">
                                <div class="row">
                                    <?php while ( $loopDesktop->have_posts() ) : $loopDesktop->the_post();
                                        $thumb_id = get_post_thumbnail_id();
                                        $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
                                        $styles = $thumb_url[0] != "" && $thumb_url[0] != "http://haven.local/wp/wp-includes/images/media/default.png" ?
                                            "background-image: url(". $thumb_url[0] ."); background-size: cover;" : "background: url(/app/themes/BBTFramework/images/logo-white.png) center center no-repeat #00bce7; background-size: 132px 33px;"?>
                                        <div class="col-md-6 post-image-wrapper">
                                            <a href="<?php the_permalink(); ?>" style="<?php echo $styles; ?>">
                                                <div class="post-image"></div>
                                            </a>
                                            <div class="post-content-wrapper">
                                                <div class="row post-social">
                                                    <div class="col-8">
                                                        <div class="post-date"><?php the_time( 'j F Y' ); ?></div><div class="post-category"><?php the_category(); ?></div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="post-social-wrap">
                                                            <i class="material-icons share-trig">share</i>
                                                            <div class="post-social">Share: <div class="button st-custom-button" data-network="email"><i class="material-icons">email</i></div><div class="button st-custom-button" data-network="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                                <div class="post-content"><?php echo excerpt(25) ?></div>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                                <div class="row pb-5">
                                    <div class="col-md-12 pagination_wrapper post-pagination1 ml-3 justify-content-center">
                                        <div class="pagination_links">
                                            <?php
                                            $big2 = 999999999; // need an unlikely integer
                                            $maxMobile = ceil($loopDesktop->max_num_pages / $posts_per_page);

                                            echo paginate_links( array(
                                                'base' => preg_replace('/\?.*/', '/', get_pagenum_link()) . '%_%',
                                                'format' => '?paged=%#%',
                                                'current' => max( 1, $paged ),
                                                'total' => $maxMobile
                                            ) );
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif;
                        wp_reset_query();
                        wp_reset_postdata(); ?>

                        <div class="col-lg-4 col-md-12 sidebar">
                            <?php
                            $popularpost = new WP_Query( array(
                                'posts_per_page' => 3,
                                'meta_key' => 'wpb_post_views_count',
                                'orderby' => 'meta_value_num',
                                'order' => 'DESC'  )
                            );

                            if($popularpost->have_posts()) : ?>
                                <div class="articles">
                                    <div class="title"><span>Most read articles</span></div>
                                    <div class="contents blog-gallery">
                                        <?php while ( $popularpost->have_posts() ) : $popularpost->the_post();
                                            $thumb_id = get_post_thumbnail_id();
                                            $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
                                            $styles = $thumb_url[0] != "" && $thumb_url[0] != "http://haven.local/wp/wp-includes/images/media/default.png" ?
                                                "background-image: url(". $thumb_url[0] .")" : "background: url(/app/themes/BBTFramework/images/logo-white.png) center center no-repeat #00bce7; 
                                            background-size: 60px auto;"; ?>

                                            <div class="item row no-gutters d-flex align-items-top">
                                                <div class="col-lg-3 col-md-12 col-sm-12">
                                                    <a href="<?php the_permalink(); ?>">
                                                        <div class="image" style="<?php echo $styles; ?>">
                                                            <div class="post-image"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="col-lg-9 col-md-10 col-sm-12 contents">
                                                    <a href="<?php the_permalink(); ?>">
                                                        <h5><?php the_title(); ?></h5>
                                                    </a>
                                                    <a href="<?php the_permalink(); ?>">
                                                        <p><?php the_time('d.m.Y'); ?></p>
                                                    </a>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                </div>
                            <?php endif;
                            wp_reset_postdata();
                            wp_reset_query(); ?>

                            <div class="newsletter desktop d-none d-lg-block d-xs-block">
                                <?php $title = get_field('sbt_title', 'options'); ?>
                                <div class="title"> <?php echo $title != "" ? $title : "Stay up to date"; ?></div>
                                <p><?php echo get_field('sbt_subtitle', 'options'); ?></p>
                                <?php gravity_form( 1, false, false, false, '', false ); ?>
                            </div>

                        </div><!-- /#main-sidebar-container -->

                    </div>
                </div>
            </div>

        <?php woo_loop_after(); ?>
    </div>
    </div>
    </section><!-- /#main -->

<?php woo_main_after(); ?>


    </div><!-- /#content -->
<?php woo_content_after(); ?>

<?php get_footer(); ?>