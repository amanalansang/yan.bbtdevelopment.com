<?php 
add_action( 'wp_enqueue_scripts', 'enqueue_kiwisaver_style' );
function enqueue_kiwisaver_style() {
    //Style
    if( is_page_template('template-kiwisavercalc.php') ) {
        wp_enqueue_style( 'kiwisaver-calc', get_stylesheet_directory_uri() . '/css/kiwisaver-calculator.css' );
    }
    //scripts
    if( is_page_template('template-kiwisavercalc.php') ) {
        wp_enqueue_script( 'chart-js', get_stylesheet_directory_uri() . '/js/calculators/kiwisaver/chart.js', array( 'jquery' ) );
        wp_enqueue_script( 'chart-js-style', get_stylesheet_directory_uri() . '/js/calculators/kiwisaver/chartjs-plugin-style.js', array( 'jquery' ) );
        wp_enqueue_script( 'form-validation', '//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js', array( 'jquery' ) );
        wp_enqueue_script( 'numeral', '//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'kiwi-main', get_stylesheet_directory_uri() . '/js/calculators/kiwisaver/main.js', array( 'jquery' ), '', true );
    }
} 
//Remove scripts
function wp_67472455() {
   wp_dequeue_style( 'animate' );
   wp_dequeue_style( 'bxslider' );
}
add_action( 'wp_print_styles', 'wp_67472455', 100 );


add_shortcode('kiwisaver_calc', 'kiwisaver_calc');

function kiwisaver_calc() {
    //Custom Fields
    $WageGrowth = get_field('wage_growth');
    $CPIAgainstReturns = get_field('cpi_against_returns');

    $DefensiveGrossReturns = get_field('defensive_gross_returns');
    $DefensiveFees = get_field('defensive_fees');

    $ConservativeGrossReturns = get_field('Conservative_gross_returns');
    $ConservativeFees = get_field('Conservative_fees');

    $BalancedGrossReturns = get_field('Balanced_gross_returns');
    $BalancedFees = get_field('Balanced_fees');

    $GrowthGrossReturns = get_field('Growth_gross_returns');
    $GrowthFees = get_field('Growth_fees');

    $AggressiveGrossReturns = get_field('Aggressive_gross_returns');
    $AggressiveFees = get_field('Aggressive_fees');
    
    $text ='';
    $text .='
        <div class="container" id="kiwisaver-calc">
            <div class="row">
                <div class="col-md-12">
                    <h2>How much could you save?</h2>
                </div>
            </div>
            <div class="row form">
                <div class="col-md-12">
                    <form action="" type="POST" id="theForm">
                        <input type="hidden" name="WageGrowth" id="WageGrowth" value="'.$WageGrowth.'">
                        <input type="hidden" name="CPIAgainstReturns" id="CPIAgainstReturns" value="'.$CPIAgainstReturns.'">
                        <input type="hidden" name="DefensiveGrossReturns" id="DefensiveGrossReturns" value="'.$DefensiveGrossReturns.'">
                        <input type="hidden" name="DefensiveFees" id="DefensiveFees" value="'.$DefensiveFees.'">
                        <input type="hidden" name="ConservativeGrossReturns" id="ConservativeGrossReturns" value="'.$ConservativeGrossReturns.'">
                        <input type="hidden" name="ConservativeFees" id="ConservativeFees" value="'.$ConservativeFees.'">
                        <input type="hidden" name="BalancedGrossReturns" id="BalancedGrossReturns" value="'.$BalancedGrossReturns.'">
                        <input type="hidden" name="BalancedFees" id="BalancedFees" value="'.$BalancedFees.'">
                        <input type="hidden" name="GrowthGrossReturns" id="GrowthGrossReturns" value="'.$GrowthGrossReturns.'">
                        <input type="hidden" name="GrowthFees" id="GrowthFees" value="'.$GrowthFees.'">
                        <input type="hidden" name="AggressiveGrossReturns" id="AggressiveGrossReturns" value="'.$AggressiveGrossReturns.'">
                        <input type="hidden" name="AggressiveFees" id="AggressiveFees" value="'.$AggressiveFees.'">
                        <div class="row">
                            <div class="col-md-12">
                                <span class="in-blk">I was born in </span>
                                <select class="in-blk" name="month" id="birthMonth">
                                ';
                                    for ($itirateMonth=1; $itirateMonth<=12; $itirateMonth++) {
                                        $month = date('F', mktime(0,0,0,$itirateMonth, 1, date('Y')));
                                        $value = date('m', mktime(0,0,0,$itirateMonth, 1, date('Y')));
                                        $text .= '<option value="'.$value.'"';
                                        if ($value == "1") {
                                            $text .="selected";
                                        }
                                        $text .= ' >'.$month.'</option>';
                                    }
                        $text .='</select>
                                <select class="in-blk" name="year" id="birthYear">
                                    <option value="">  </option>';
                                    $MaxYear = date("Y") - 65 ;
                                    for ($year=$MaxYear; $year<=date("Y"); $year++) {
                                        $text .= '<option value="'.$year.'"';
                                        if ($year == 1970) {
                                            $text .="selected";
                                        }
                                        $text .= '>'.$year.'</option>';

                                    }
                        $text .='</select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="in-blk">My annual income is</span>
                                <input type="text" class="text width-dynamic currency-mask" id="annual-income" name="income" value="$65,000"    placeholder="$10,000">
                                <span class="in-blk">and my current KiwiSaver balance is</span>
                                <input type="text" class="number width-dynamic currency-mask" name="kiwisaver-balance" id="kiwisaver-balance" value="$15,000"    placeholder="$10,000">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <span class="in-blk">I will contribute</span>
                                <select class="in-blk" name="my-contribution" id="my-contribution">
                                    <option value="0.03">3%</option>
                                    <option value="0.04">4%</option>
                                    <option value="0.06">6%</option>
                                    <option value="0.08">8%</option>
                                    <option value="0.1">10%</option>
                                </select>
                                <span class="in-blk">of my income  and my employer will contribute</span>
                                <select class="in-blk" name="employer-contribution" id="employer-contribution">
                                    <option value="0.03">3%</option>
                                    <option value="0.04">4%</option>
                                    <option value="0.06">6%</option>
                                    <option value="0.08">8%</option>
                                    <option value="0.1">10%</option>
                                </select> on top.
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                            Please show my results 
                            <select class="in-blk" name="inflation"  id="inflation">
                                <option value="1">with</option>
                                <option value="0" selected>without</option>
                            </select>
                            inflation.
                        </div>
                        </div>
                        <div class="row d-none">
                            <input type="submit" value="Go!">
                        </div>
                    </form>
                </div>
            </div>
            <div class="row chart ">
                <div class="chart-container col-12">
                    <canvas id="theChart" width=100% ></canvas>
                </div>
            </div>
        </div>
    ';
    return $text;
}?>