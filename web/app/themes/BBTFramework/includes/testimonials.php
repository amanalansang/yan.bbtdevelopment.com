<div class="testimonials row">
    <?php
        $loop = new WP_Query( array(
            'post_type' => 'testimonial',
            'posts_per_page' => -1
        ) );
        $count = 1;
        ?>
        <?php
        if ( $loop->have_posts() ) :
            while ( $loop->have_posts() ) : $loop->the_post();
                $testi_first_name = get_field("testi_first_name");
                $testi_last_name = get_field("testi_last_name");
                $testi_your_location = get_field("testi_your_location");
                $testi_your_adviser = get_field("testi_your_adviser");
                $testi_services = get_field("testi_services");
                $testimonial = get_field("testimonial");
                $testi_star_rating = get_field("testi_star_rating");
                ?>
                <div class="testimonial col-md-4">
                    <div class="testimonial__inner">
                        <img src="/app/themes/BBTFramework/images/testi_quote.png" class="testimonial__icon">
                        <div class="testimonial__name"><?php echo $testi_first_name; ?> <?php echo $testi_last_name; ?></div>
                        <div class="testimonial__location"><?php echo $testi_your_location; ?></div>
                        <div class="testimonial__rating"><img src="/app/themes/BBTFramework/images/rating_<?php echo $testi_star_rating; ?>.png"></div>
                        <div class="testimonial__review"><?php echo $testimonial; ?></div>
                    </div>
                </div>
                <?php if( $count % 3 == 0):  //if this is the 3rd post then clear! ?>
                    blah
                <?php
                    $count++;
                endif; ?>
            <?php endwhile;
        endif;
        wp_reset_postdata();
    ?>
    <div class="col-md-12 px-0">
        <a href="#" class="load__more">Load More</a>
    </div>
</div>