<?php
//dequeue scripts
function wpdocs_dequeue_script()
{
    wp_dequeue_style('career-style');
}

add_action('wp_print_scripts', 'wpdocs_dequeue_script', 999);


function async_scripts($url)
{
    if (strpos($url, '#asyncload') === false) {
        return $url;
    } else {
        if (is_admin()) {
            return str_replace('#asyncload', '', $url);
        } else {
            return str_replace('#asyncload', '', $url) . "' async='async";
        }
    }
}

add_filter('clean_url', 'async_scripts', 11, 1);


function add_async_attribute($tag, $handle)
{
    // add script handles to the array below
    $scripts_to_async = array();
    if (is_front_page()) {
        $scripts_to_async = array(
            'videos',
            'career-script',
            'gform_json',
            'gform_placeholder',
            'third-party',
            'modernizr',
            'general'
        );
    }

    foreach ($scripts_to_async as $async_script) {
        if ($async_script === $handle) {
            return str_replace(' src', ' async="async" src', $tag);
        }
    }

    return $tag;
}

add_filter('script_loader_tag', 'add_async_attribute', 10, 2);


function add_style_async_attribute($tag, $handle)
{
    // add script handles to the array below

    $styles_to_async = array();
    if (is_front_page()) {
        $styles_to_async = array(
            'career-style',
            'bootstrap-css',
            'reset-megamenu',
            'style-megamenu',
            'jquery-ui',
            'jquery-ui-theme',
            'custom',
            'footer',
            'search-header',
            'parent-style',
            'js_composer_front',
            'gforms_reset_css',
            'gforms_formsmain_css',
            'gforms_ready_class_css',
            'gforms_browsers_css',
            'theme-stylesheet',
            'woo-gravity-forms',
            'popup-messenger',
            'messenger'
        );
    }

    foreach ($styles_to_async as $async_style) {
        if ($async_style === $handle) {
            return str_replace(' href', ' async="async" href', $tag);
        }
    }
    return $tag;

}

add_filter('style_loader_tag', 'add_style_async_attribute', 10, 2);



function submit_noptimize() {
  $data = true;
  if (is_front_page()) {
    $data = false;
  }

  return $data;
}

add_filter('autoptimize_filter_noptimize','submit_noptimize',10,0);