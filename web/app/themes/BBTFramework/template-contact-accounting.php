<?php
/**
 * Template Name: Accounting Contact Page
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a page ('page' post_type) unless another page template overrules this one.
 * @link http://codex.wordpress.org/Pages
 *
 * @package WooFramework
 * @subpackage Template
 */

 get_header();
 if (isset($_GET['submit'])) {
     $submit = $_GET['submit'];
     if ($submit == "success") { ?>
         <style type="text/css">
             .page-contact-our-accounting-team #contact-send { display: none }
             .page-contact-our-accounting-team #success-contact { display: block !important; }
         </style>
     <?php }
 } ?>
    <!-- #content Starts -->
    <?php woo_content_before(); ?>

    <div class="content">
        <section id="main">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; endif;
            wp_reset_postdata(); ?>
        </section><!-- /#main -->
    </div>
    <?php woo_content_after(); ?>

<?php get_footer(); ?>