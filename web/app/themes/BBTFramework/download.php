<?php
/**
 * Template Name: Download PDF
 */

// $file = $_GET['file'];
// header('Content-Type: application/pdf');
// header('Content-Disposition: attachment; filename="' . basename($file) . '"');
// header('Content-Length: ' . $file);
// readfile($filename);
// die();


// if(isset($_REQUEST["file"])){
    
//     $file = urldecode($_REQUEST["file"]); // Decode URL-encoded string
	
	$file = $_GET['file'];
	header("Content-Disposition: attachment; filename=" . basename( $file ));   
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Description: File Transfer");            
	header("Content-Length: " . Filesize( $file ));
	flush(); // this doesn't really matter.
	$fp = fopen($file, "r");
	while (!feof($fp))
	{
	    echo fread($fp, 65536);
	    flush(); // this is essential for large downloads
	} 
	fclose($fp);
?>