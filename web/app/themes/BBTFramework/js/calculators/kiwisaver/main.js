/*global
$, jQuery, window
*/
jQuery(document).ready(function ($) {
    "use strict";
    //PIR Related
    const PIR_DEFAULT = 0.105;
    const PIR_MIN_SALARY = 14000;
    const PIR_MAX_SALARY = 48000;
    //TAX CREDIT Related
    const TAX_CREDIT_LIMIT = 1042.86;
    //Get Tax Related
    const TAX_RATE = 0.105;
    const TAX_RATE_SALARY_LIMIT1 = 16800;
    const TAX_RATE_SALARY_LIMIT2 = 57600;
    const TAX_RATE_SALARY_LIMIT3 = 84000;
    //SUBMIT Related
    const AGE_LIMIT = 65;
    const VOLUNTARY_CONTRIBUTION = 0;
    /*
    * Refer to the spreadsheet D17
    */
    function getPIR(salary) {
        let pir = PIR_DEFAULT;
        if (salary > PIR_MIN_SALARY) {
            pir = 0.175;
        }
        if (salary > PIR_MAX_SALARY) {
            pir = 0.28;
        }
        return pir;
    }
    /*
    * Refer to the spreadsheet D13
    */
    function getContribution(employeeContribution, A10, taxCredit, voluntaryContributions) {
        return employeeContribution + A10 + taxCredit + voluntaryContributions;
    }
    /*
    * Refer to the spreadsheet D11
    */
    function getTaxCredit(employeeContribution, voluntaryContribution) {
        let taxCredit;
        taxCredit = (employeeContribution + voluntaryContribution) / 2;
        if ((employeeContribution + voluntaryContribution) > TAX_CREDIT_LIMIT) {
            taxCredit = 521.43; //Default tax credit
        }
        return taxCredit;
    }
    /*
    * Refer to the spreadsheet D9
    */
    function getTax(salary, employerContribution) {
        let taxRate;
        taxRate = TAX_RATE;
        if ((salary + employerContribution) > TAX_RATE_SALARY_LIMIT1) {
            taxRate = 0.175;
        }
        if ((salary + employerContribution) > TAX_RATE_SALARY_LIMIT2) {
            taxRate = 0.3;
        }
        if ((salary + employerContribution) > TAX_RATE_SALARY_LIMIT3) {
            taxRate = 0.33;
        }
        return taxRate * employerContribution;
    }

    function getBirthDate(year, month) {
        let dob;
        let today;

        dob = year + '-' + month + '-01';
        dob = new Date(dob);
        today = new Date();

        return Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
    }
    /*
    * Refer to the spreadsheet D14
    */
    function getAge(ageLimit) {
        //return getBirthDate($("#birthYear").val(), $("#birthMonth").val())
        return ageLimit - getBirthDate($("#birthYear").val(), $("#birthMonth").val());
    }

    function getPercentage(n) {
        return parseFloat(n) / 100;
    }
    /*
    * Refer to the spreadsheet G6,H6,I6,J6,K6,
    */
    function getNetReturnAfterPIR(gross, fees, pir) {
        let num;
        num = gross * (1 - pir) * (1 - fees);
        return parseFloat(num);
    }
    /*
    * Refer to the spreadsheet G13,H13,I13,J13,K13,
    */
    function getTotalResultNoInflation(C5, G6, D13, D15, D14) {
        /*Variable init*/
        let set1Part1;
        let set1Part2;
        let set1Part3;
        let set1Part4;
        let set1;
        let set2;

        set1Part1 = Math.pow((1 + D15), D14);
        set1Part2 = Math.pow((1 + G6), D14);
        set1Part3 = D15 - G6;
        set1Part4 = (set1Part1 - set1Part2) / set1Part3;

        set1 = set1Part4 * D13;
        set2 = (C5) * (Math.pow((1 + G6), D14));

        return Math.round((set1 + set2) / 1000) * 1000; //round to nearest thousand
    }
    /*
    * Refer to the spreadsheet G14,H14,I14,J14,K14,
    */
    function getTotalResultWithInflation(G13, D16, D14) {
        let set3;
        set3 = G13 / (Math.pow((1 + D16), D14));
        return Math.round(set3 / 1000) * 1000; //round to nearest thousand
    }
    function convertToInt(currency) {
        let newCurrency;
        newCurrency = currency.replace('$', '');
        return parseInt(newCurrency.replace(/\,/g, ''));
    }

    /************************************************************************************************/
    /*******                                Currency formatting                               *******/
    /************************************************************************************************/
    function formatNumber(n) {
        // format number 1000000 to 1,234,567
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    //$(".currency-mask").maskMoney({prefix:'$ ', allowNegative: true, thousands:',', decimal:'.', affixesStay: true});
    $(".currency-mask").on('keyup', function() {
        let getFieldValue = $(this).val();
        let newFieldValue = numeral(getFieldValue).format('$0,0');

        $(this).val(newFieldValue);
        console.log(  );
    });
    /************************************************************************************************/
    /*******                                AUTO WIDTH INPUT                                  *******/
    /************************************************************************************************/
    $.fn.textWidth = function (text, font) {
        if (!$.fn.textWidth.fakeEl) {
            $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
        }
        $.fn.textWidth.fakeEl.text(text || this.val() || this.text() || this.attr('placeholder')).css('font', font || this.css('font'));
        return $.fn.textWidth.fakeEl.width();
    };
    let dynamicWidth;
    dynamicWidth = $('.width-dynamic');
    dynamicWidth.on('input', function () {
        let inputWidth;
        inputWidth = $(this).textWidth();
        $(this).css({
            width: inputWidth + 15 //15 for extra space
        });
    }).trigger('input');
    function inputWidth(elem) {
        elem = $(this);
        return elem;
    }
    inputWidth(dynamicWidth);

    /************************************************************************************************/
    /*******                           CHART STYLING RELATED SCRIPTS                          *******/
    /************************************************************************************************/
    function nFormatter(num, digits) {
        let symbolSet;
        let regex;

        symbolSet = [
            {value: 1, symbol: ''},
            {value: 1E3, symbol: 'k'},
            {value: 1E6, symbol: 'M'},
            {value: 1E9, symbol: 'G'},
            {value: 1E12, symbol: 'T'},
            {value: 1E15, symbol: 'P'},
            {value: 1E18, symbol: 'E'},
        ];
        regex = /\.0+$|(\.[0-9]*[1-9])0+$/;

        let i;
        for (i = symbolSet.length - 1; i > 0; i -= 1) {
            if (num >= symbolSet[i].value) {
                break;
            }
        }
        let nFormat;
        nFormat = (num / symbolSet[i].value);
        return nFormat.toFixed(digits).replace(regex, "$1") + symbolSet[i].symbol;
    }
    function tooltipTitle(num) {
        let tooltipTitle = formatNumber(num);
        if ($(window).outerWidth() < 768) {
            tooltipTitle = nFormatter(num, 1);
        }
        return '$' + tooltipTitle;
    }
    function chartResponsiveVal(desktop, mobile) {
        let ResponsiveVal;
        ResponsiveVal = desktop;
        if ($(window).outerWidth() < 768) {
            ResponsiveVal = mobile;
        }
        return ResponsiveVal;
    }
    function chartMaxResponsive(max, additional) {
        let MaxResponsive;
        MaxResponsive = Math.max.apply(Math, max) +
            (Math.max.apply(Math, max) * additional);
        if ($(window).outerWidth() < 768) {
            MaxResponsive = Math.max.apply(Math, max) + (Math.max.apply(Math, max) * (additional + 0.1));
        }
        return MaxResponsive;
    }
    /************************************************************************************************/
    /*******                                LOADING CHART                                     *******/
    /************************************************************************************************/
    function loadNewChart(dataArray = []) {
        let $data;
        let ctx;
        let theChart;

        ctx = document.getElementById('theChart').getContext('2d');
        $data = dataArray;

        if ($.isEmptyObject(dataArray)) {
            //trigger form to get load data from current fields
            $("#theForm").trigger('change');
        }

        //  Setting dimension before render

        theChart = $("#theChart");
        theChart.css('height', '350px');
        if ($(window).outerWidth() < 768) {
            theChart.css('height', '305px');
            theChart.css('width', '100%');
        }
        /************* Customize Tooltip ****************/
        Chart.pluginService.register({
            beforeRender: function (chart) {
                if (chart.config.options.showAllTooltips) {
                    // create an array of tooltips
                    // we can't use the chart tooltip because there is only one tooltip per chart
                    chart.pluginTooltips = [];
                    chart.config.data.datasets.forEach(function (dataset, i) {
                    chart.getDatasetMeta(i).data.forEach(function (sector) {
                        chart.pluginTooltips.push(new Chart.Tooltip({
                            _chart: chart.chart,
                            _chartInstance: chart,
                            _data: chart.data,
                            _options: chart.options.tooltips,
                            _active: [sector]
                            }, chart));
                        });
                    });
                  // turn off normal tooltips
                  chart.options.tooltips.enabled = false;
                }
            },
            afterDraw: function (chart, easing) {
                if (chart.config.options.showAllTooltips) {
                  // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                    if (!chart.allTooltipsOnce) {
                        if (easing !== 1) {
                            return;
                        }
                        chart.allTooltipsOnce = true;
                    }
                  // turn on tooltips
                    chart.options.tooltips.enabled = true;
                Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                        tooltip.initialize();
                        tooltip._model.shadowBlur = 10;
                        tooltip._model.shadowColor = 'rgba(0, 0, 0, 0.5)';
                        tooltip._model.shadowOffsetX = 3;
                        tooltip._model.shadowOffsetY = 3;

                        tooltip.update();
                        tooltip.pivot();
                        tooltip.draw();
                    });
                  chart.options.tooltips.enabled = false;
                }
            }
        });

        let desktopLabel;
        let MobileLabel;
        let chartData;

        desktopLabel = [
            'Defensive Fund',
            'Conservative Fund',
            'Balanced Fund',
            'Growth Fund',
            'Aggressive Fund'];
        MobileLabel = [
            ['Defensive', 'Fund'],
            ['Conservative', 'Fund'],
            ['Balanced', 'Fund'],
            ['Growth', 'Fund'],
            ['Aggressive', 'Fund']];
        chartData = {
            labels: chartResponsiveVal(desktopLabel, MobileLabel),
            datasets: [
                {
                    label: '',
                    backgroundColor: [
                        'rgba(203, 245, 255, 0.9)',
                        'rgba(170, 235, 250, 0.9)',
                        'rgba(123, 225, 248, 0.9)',
                        'rgba(53, 214, 251, 0.9)',
                        'rgba(0, 188, 231, 0.9)',
                    ],
                    borderSkipped: 'top',
                    data: $data,
                }],
        };
        let chartOptions = {
            maintainAspectRatio: false,
            responsive: true,
            showAllTooltips: true,
            options: {events: []},
            legend: {
                display: false,
            },
            tooltips: {
                backgroundColor: '#fbfbfb',
                borderColor: 'rgba(0, 0, 0, 0.15)',
                caretSize: 0,
                cornerRadius: 18,
                footerFontSize: 12,
                shadowBlur: 10,
                shadowColor: 'rgba(0, 0, 0, 0.5)',
                shadowOffsetX: 3,
                shadowOffsetY: 3,
                titleFontColor: '#006bc0',
                titleFontStyle: '600',
                titleMarginBottom: 0,
                xAlign: 'center',
                xPadding: chartResponsiveVal(15, 5),
                yPadding: 10,
                callbacks: {
                    labelTextColor: function() {
                        return '#006bc0';
                    },
                    title: function(tooltipItem) {
                        return tooltipTitle(tooltipItem[0].value);
                    },
                    label: function() {
                        return false;
                    },
                },
            },
            scales: {
                xAxes: [
                    {
                        barPercentage: 0.9,
                        categoryPercentage: 1,
                        gridLines: {
                            display: false,
                            tickMarkLength: 20,
                        },
                        ticks: {
                            maxRotation: 0,
                        },
                    }],
                yAxes: [
                    {
                        gridLines: {
                            color: '#979797',
                            drawBorder: false,
                            drawTicks: false,
                        },
                        ticks: {
                            display: chartResponsiveVal(true, false),
                            fontFamily: 'National-Book',
                            fontSize: chartResponsiveVal(14, 12),
                            max: chartMaxResponsive($data, 0.1),
                            min: 0,
                            padding: 20,
                            stepSize: Math.max.apply(Math, $data) / 4,
                            callback: function(value) { // Include a dollar sign in the ticks
                                return '$' + nFormatter(value, 2);
                            },
                        },
                    }],
            },
        };

        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: chartData,
            options: chartOptions
        });
    }

    /*
    * Start Form action
    */

    function TheFormCalculation(){

        //Calculator Variables
        let AGE = getAge(AGE_LIMIT);
        let SALARY = convertToInt($('#annual-income').val());
        let BALANCE = convertToInt($('#kiwisaver-balance').val());
        let EMPLOYEE_CONTRIBUTION_RATE = SALARY * $('#my-contribution').val();
        let EMPLOYER_CONTRIBUTION_RATE = SALARY * $("#employer-contribution").val();
        let TAX = getTax(SALARY, EMPLOYER_CONTRIBUTION_RATE);
        let A10 = EMPLOYER_CONTRIBUTION_RATE - TAX;
        let TAX_CREDIT = getTaxCredit(EMPLOYEE_CONTRIBUTION_RATE, VOLUNTARY_CONTRIBUTION);
        let CONTRIBUTION = getContribution(EMPLOYEE_CONTRIBUTION_RATE, A10, TAX_CREDIT, VOLUNTARY_CONTRIBUTION);
        let PIR = getPIR(SALARY);
        let WAGE_GROWTH = getPercentage($("#WageGrowth").val());
        let CPI_AGAINST_RETURNS = getPercentage($("#CPIAgainstReturns ").val());
        //Defensive
        let DEFENSIVE_GROSS_RETURNS = getPercentage($("#DefensiveGrossReturns").val());
        let DEFENSIVE_FEES = getPercentage($("#DefensiveFees").val());
        let DEFENSIVE_NET_RETURN = getNetReturnAfterPIR(DEFENSIVE_GROSS_RETURNS, DEFENSIVE_FEES, PIR);
        //Conservative
        let CONSERVATIVE_GROSS_RETURNS = getPercentage($("#ConservativeGrossReturns").val());
        let CONSERVATIVE_FEES = getPercentage($("#ConservativeFees").val());
        let CONSERVATIVE_NET_RETURN = getNetReturnAfterPIR(CONSERVATIVE_GROSS_RETURNS, CONSERVATIVE_FEES, PIR);
        //Balanced
        let BALANCED_GROSS_RETURNS = getPercentage($("#BalancedGrossReturns").val());
        let BALANCED_FEES = getPercentage($("#BalancedFees").val());
        let BALANCED_NET_RETURN = getNetReturnAfterPIR(BALANCED_GROSS_RETURNS, BALANCED_FEES, PIR);
        //Growth
        let GROWTH_GROSS_RETURNS = getPercentage($("#GrowthGrossReturns").val());
        let GROWTH_FEES = getPercentage($("#GrowthFees").val());
        let GROWTH_NET_RETURN = getNetReturnAfterPIR(GROWTH_GROSS_RETURNS, GROWTH_FEES, PIR);
        //Aggressive
        let AGGRESSIVE_GROSS_RETURNS = getPercentage($("#AggressiveGrossReturns").val());
        let AGGRESSIVE_FEES = getPercentage($("#AggressiveFees").val());
        let AGGRESSIVE_NET_RETURN = getNetReturnAfterPIR(AGGRESSIVE_GROSS_RETURNS, AGGRESSIVE_FEES, PIR);
        //OVERALL RESULT OF THE COMPUTATION
        //Defensive
        let RESULT_DEFENSIVE_NON_INFLATION = getTotalResultNoInflation(BALANCE, DEFENSIVE_NET_RETURN, CONTRIBUTION, WAGE_GROWTH, AGE);
        let RESULT_DEFENSIVE_WITH_INFLATION = getTotalResultWithInflation(RESULT_DEFENSIVE_NON_INFLATION, CPI_AGAINST_RETURNS, AGE);
        //Conservative
        let RESULT_CONSERVATIVE_NON_INFLATION = getTotalResultNoInflation(BALANCE, CONSERVATIVE_NET_RETURN, CONTRIBUTION, WAGE_GROWTH, AGE);
        let RESULT_CONSERVATIVE_WITH_INFLATION = getTotalResultWithInflation(RESULT_CONSERVATIVE_NON_INFLATION, CPI_AGAINST_RETURNS, AGE);
        //Balanced
        let RESULT_BALANCED_NON_INFLATION = getTotalResultNoInflation(BALANCE, BALANCED_NET_RETURN, CONTRIBUTION, WAGE_GROWTH, AGE);
        let RESULT_BALANCED_WITH_INFLATION = getTotalResultWithInflation(RESULT_BALANCED_NON_INFLATION, CPI_AGAINST_RETURNS, AGE);
        //Growth
        let RESULT_GROWTH_NON_INFLATION = getTotalResultNoInflation(BALANCE, GROWTH_NET_RETURN, CONTRIBUTION, WAGE_GROWTH, AGE);
        let RESULT_GROWTH_WITH_INFLATION = getTotalResultWithInflation(RESULT_GROWTH_NON_INFLATION, CPI_AGAINST_RETURNS, AGE);
        //Aggressive
        let RESULT_AGGRESSIVE_NON_INFLATION = getTotalResultNoInflation(BALANCE, AGGRESSIVE_NET_RETURN, CONTRIBUTION, WAGE_GROWTH, AGE);
        let RESULT_AGGRESSIVE_WITH_INFLATION = getTotalResultWithInflation(RESULT_AGGRESSIVE_NON_INFLATION, CPI_AGAINST_RETURNS, AGE);

        var charData = [RESULT_DEFENSIVE_WITH_INFLATION, RESULT_CONSERVATIVE_WITH_INFLATION, RESULT_BALANCED_WITH_INFLATION, RESULT_GROWTH_WITH_INFLATION, RESULT_AGGRESSIVE_WITH_INFLATION];
        if ($("#inflation").val() === "0") {
            charData = [RESULT_DEFENSIVE_NON_INFLATION, RESULT_CONSERVATIVE_NON_INFLATION, RESULT_BALANCED_NON_INFLATION, RESULT_GROWTH_NON_INFLATION, RESULT_AGGRESSIVE_NON_INFLATION];
        }
        /************************************************************************************************/
        /*******                                                                                  *******/
        /*******                                LOAD DATA TO CHART                                *******/
        /*******                                                                                  *******/
        /************************************************************************************************/

        let chartContainer;
        chartContainer = $(".chart-container");
        chartContainer .html('');
        chartContainer .html('<canvas id="theChart"></canvas>');

        return charData;
    }
    //Form Events control.
    const theForm = $("#theForm");
    theForm.submit(function (e){
        e.preventDefault();
        let theData
        theData = TheFormCalculation();

        loadNewChart( theData  );
    });

    theForm.change(function (e) {
        e.preventDefault();
        theForm.trigger('submit');
    });

    $.validate({
        form: "#kiwisaver-calc"
    });
    $(window).load(function () {
        theForm.trigger('submit');
        //loadNewChart([304000, 498000, 843000, 1432000, 1522000]);
    });
    $(window).resize(function () {
        //loadNewChart();
    });

    /************************************************************************************************/
    /*******                                ACCORDION STUFF                                   *******/
    /************************************************************************************************/
    const accordionThis = $(".accordion_this");
    if (accordionThis.length  !== 0){
        accordionThis.each(function () {
            let $this = this;
            let accordionHead = $($this).find(".accordion_head");
            let accordionContent = $($this).find(".accordion_content");
            let btnText = $($this).find(".accordion_head.mobile").text();
            accordionHead.append('<div class="accordion_btn trgr"></div>');
            $($this).find(".trgr").click(function () {
                accordionContent.slideToggle(function (){
                    accordionHead.removeClass("open");
                    $($this).find('.accordion_head.mobile').text(btnText.replace('Hide', 'Tap to read'));
                    $('html, body').animate({
                        scrollTop: $(".chart-container").offset().top
                    }, 2000);
                    if ($(this).is(':visible')) {
                        accordionHead.addClass("open");
                        if ($(window).width() < 991 ) {
                            $($this).find('.accordion_head.mobile').text(btnText.replace('Tap to read', 'Hide'));

                        }
                    }
                });
            });
        });
    }

});

