jQuery(document).ready( function($){
    $(window).bind('scroll', function () {
        var stickyHeaderTop = $('.top-container').height();

    if ($(window).scrollTop() > stickyHeaderTop + 100) {
            $('.page-jobs #mobile_header h3').addClass('scrolling');
        } else {
            $('.page-jobs #mobile_header h3').removeClass('scrolling');                  
        }
    })
    $(".page-jobs .cd-dropdown-trigger").click( function(){
        $('.page-jobs #mobile_header h3').toggleClass('active-menu');
    })
})
