/*global
$, jQuery, window*/
jQuery(document).ready(function ($) {
    "use strict";
    if ($('.page-our-story').length) {

        $('<div class="top-menu-dropdown">Learn More<i class="material-icons">arrow_drop_down</i></div>').insertBefore('#navigation-about ul');
        $('.top-menu-dropdown').on('click', function (e) {
            e.preventDefault();
            $(this).find('i').toggleClass('active');
            $(this).next().toggleClass('showThis');
        });

        $("ul.mobile-map-select").on("click", ".init", function () {
            $(this).closest("ul.mobile-map-select").children('li:not(.init)').toggle();
        });

        var allOptions = $("ul.mobile-map-select").children('li:not(.init)');
        $("ul.mobile-map-select").on("click", "li:not(.init)", function () {
            allOptions.removeClass('selected');
            $(this).addClass('selected');
            $("ul.mobile-map-select").children('.init').html($(this).html());
            allOptions.toggle();

            var current = $(this).attr('id');
            $('.map .area').each(function () {
                var str = $(this).attr('alt').toLowerCase();
                str = str.replace(/\s+/g, '-');
                if (str === current) {
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');
                }
            });
        });

        if (jQuery(window).width() > 1024) {
            var flag = true;
            jQuery(window).scroll(function () {
                var scrollPos = jQuery(window).scrollTop();
                if (scrollPos > jQuery('#our-story').outerHeight() - jQuery('header').outerHeight()) {
                    jQuery('.header-about').addClass('fixed').css('top', jQuery('header').outerHeight());
                    jQuery('#header').addClass('lighten');
                } else {
                    jQuery('.header-about').removeClass('fixed').css('top', 'unset');
                    jQuery('#header').removeClass('lighten');
                }

                jQuery('.header-about ul li').each(function () {
                    jQuery(this).removeClass('highlight');
                });
                // if (scrollPos > jQuery('#our-story').offset().top - (jQuery('header').outerHeight() + jQuery('.header-about').outerHeight())) {
                //     jQuery('#menu-item-4137').addClass('highlight');
                //     jQuery('#menu-item-4137').siblings().removeClass('highlight');
                // }
                if (scrollPos > jQuery('#our-values').offset().top - (jQuery('header').outerHeight() + jQuery('.header-about').outerHeight())) {
                    // jQuery('#menu-item-4138').addClass('highlight');
                    // jQuery('#menu-item-4138').siblings().removeClass('highlight');
                    jQuery('.page-our-story .story-info .item').addClass('fade-in-bottom').css('visibility', 'visible');
                }
                if (scrollPos > jQuery('#our-achievements').offset().top - (jQuery('header').outerHeight() + jQuery('.header-about').outerHeight())) {
                    // jQuery('#menu-item-4139').addClass('highlight');
                    // jQuery('#menu-item-4139').siblings().removeClass('highlight');
                    if (flag) {
                        $('#haven-years').countTo({from: 1, to: 10, speed: 3500, refreshInterval: 50});
                        $('#haven-advisers').countTo({from: 1000, to: 35, speed: 3500, refreshInterval: 50});
                        $('#haven-kms').countTo({from: 1, to: 750000, speed: 3500, refreshInterval: 50});
                        $('#haven-clients').countTo({from: 1, to: 14000, speed: 3500, refreshInterval: 50});
                        $('#haven-claims').countTo({from: 1, to: 25, speed: 3500, refreshInterval: 50});
                        $('#haven-business').countTo({from: 2000, to: 1400, speed: 3500, refreshInterval: 50});
                        flag = false;
                    }
                }
                if (scrollPos > jQuery('#our-team').offset().top - (jQuery('header').outerHeight() + jQuery('.header-about').outerHeight())) {
                    // jQuery('#menu-item-4140').addClass('highlight');
                    // jQuery('#menu-item-4140').siblings().removeClass('highlight');
                    jQuery('.map .area').addClass('scale-up-bottom');
                }
            });
        } else {
            $('.story-info .item').each(function () {
                jQuery(this).addClass('fade-in-bottom').css('visibility', 'visible');
            });
            $('.map .area').each(function () {
                $(this).css('animation-delay', 'unset');
            });
            jQuery(window).load(function () {
                var flag1 = true;
                if (flag1) {
                    $('#haven-years').countTo({from: 1, to: 10, speed: 3500, refreshInterval: 50});
                    $('#haven-advisers').countTo({from: 1000, to: 35, speed: 3500, refreshInterval: 50});
                    $('#haven-kms').countTo({from: 1, to: 750000, speed: 3500, refreshInterval: 50});
                    $('#haven-clients').countTo({from: 1, to: 14000, speed: 3500, refreshInterval: 50});
                    $('#haven-claims').countTo({from: 1, to: 25, speed: 3500, refreshInterval: 50});
                    $('#haven-business').countTo({from: 2000, to: 1400, speed: 3500, refreshInterval: 50});
                    flag1 = false;
                }
            });
        }

        if ($(window).width() < 750) {
            $('.page-our-story .story-banner').removeClass('vc_row-o-content-middle');
            $('.page-our-story .story-banner').css('minHeight', '400px');
        }
        if ($(window).width() < 480) {
            $('.page-our-story .story-banner').removeClass('vc_row-o-content-middle');
            $('.page-our-story .story-banner').css('minHeight', '500px');
        }


        /*interactive map*/
        $('.page-our-story .map .area').each(function () {
            $(this).on('click', function () {
                window.location.href = $(this).attr('href');
            });
        });
        $('.map-1').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#whangarei').addClass('activePin');
        });
        $('.map-2').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#auckland').addClass('activePin');
        });
        $('.map-3').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#pukekohe').addClass('activePin');
        });
        $('.map-4').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#hamilton').addClass('activePin');
        });
        $('.map-5').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#tauranga').addClass('activePin');
        });
        $('.map-6').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#waikato').addClass('activePin');
        });
        $('.map-7').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#rotorua').addClass('activePin');
        });
        $('.map-8').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#hawkes-bay').addClass('activePin');
        });
        $('.map-9').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#palmerston-north').addClass('activePin');
        });
        $('.map-10').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#wellington').addClass('activePin');
        });
        $('.map-11').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#nelson').addClass('activePin');
        });
        $('.map-12').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#tasman').addClass('activePin');
        });
        $('.map-13').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#christchurch').addClass('activePin');
        });
        $('.map-14').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#queenstown').addClass('activePin');
        });
        $('.map-15').on('click', function () {
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $('.map-name#invercargill').addClass('activePin');
        });

        // hover
        $(".area").on('hover', function () {
            var area_name = $(this).attr('alt');
            area_name = area_name.toLowerCase().replace(" ", "-");
            $('.map-name#'+ area_name).toggleClass('highlight');
        });

        //maplocation click
        $('#whangarei').on('click', function () {
            $('.map-1').addClass('active');
            $('.map-1').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#auckland').on('click', function () {
            $('.map-2').addClass('active');
            $('.map-2').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#pukekohe').on('click', function () {
            $('.map-3').addClass('active');
            $('.map-3').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#hamilton').on('click', function () {
            $('.map-4').addClass('active');
            $('.map-4').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#waikato').on('click', function () {
            $('.map-5').addClass('active');
            $('.map-5').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#tauranga').on('click', function () {
            $('.map-6').addClass('active');
            $('.map-6').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#rotorua').on('click', function () {
            $('.map-7').addClass('active');
            $('.map-7').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#hawkes-bay').on('click', function () {
            $('.map-8').addClass('active');
            $('.map-8').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#palmerston-north').on('click', function () {
            $('.map-9').addClass('active');
            $('.map-9').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#wellington').on('click', function () {
            $('.map-10').addClass('active');
            $('.map-10').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#nelson').on('click', function () {
            $('.map-11').addClass('active');
            $('.map-11').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#tasman').on('click', function () {
            $('.map-12').addClass('active');
            $('.map-12').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#christchurch').on('click', function () {
            $('.map-13').addClass('active');
            $('.map-13').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#queenstown').on('click', function () {
            $('.map-14').addClass('active');
            $('.map-14').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        $('#invercargill').on('click', function () {
            $('.map-15').addClass('active');
            $('.map-15').siblings().removeClass('active');
            $('.map-name').removeClass('activePin');
            $(this).addClass('activePin');
        });
        /*end interactive map*/
    }

    $(window).load(function () {
        if ($(window).width() < 750) {
            $('.page-our-story .story-banner').removeClass('vc_row-o-content-middle');
            $('.page-our-story .story-banner').css('minHeight', '400px');
        }
        if ($(window).width() < 480) {
            $('.page-our-story .story-banner').removeClass('vc_row-o-content-middle');
            $('.page-our-story .story-banner').css('minHeight', '500px');
        }
    });

    $(window).bind('orientationchange', function () {
        if ($(window).width() < 750) {
            $('.page-our-story .story-banner').removeClass('vc_row-o-content-middle');
            $('.page-our-story .story-banner').css('minHeight', '400px');
        }
        if ($(window).width() < 480) {
            $('.page-our-story .story-banner').removeClass('vc_row-o-content-middle');
            $('.page-our-story .story-banner').css('minHeight', '500px');
        }
    });

    $(window).resize(function () {
        if ($(window).width() < 750) {
            $('.page-our-story .story-banner').removeClass('vc_row-o-content-middle');
            $('.page-our-story .story-banner').css('minHeight', '400px');
        }
        if ($(window).width() < 480) {
            $('.page-our-story .story-banner').removeClass('vc_row-o-content-middle');
            $('.page-our-story .story-banner').css('minHeight', '500px');
        }
    });

    if( jQuery('.story-clients.testimonials').length ) {
        $('.story-clients.testimonials > .vc_column_container > .vc_column-inner > .wpb_wrapper').owlCarousel({
            items: 1,
            autoHeight: true,
            loop: true,
            dots: true,
            autoplay: true,
            autoplayTimeout: 2500,
            autoplayHoverPause: true
        });
    }

});


(function($) {
    $.fn.countTo = function(options) {
        // merge the default plugin settings with the custom options
        options = $.extend({}, $.fn.countTo.defaults, options || {});

        // how many times to update the value, and how much to increment the value on each update
        var loops = Math.ceil(options.speed / options.refreshInterval),
            increment = (options.to - options.from) / loops;

        return $(this).each(function() {
            var _this = this,
                loopCount = 0,
                value = options.from,
                interval = setInterval(updateTimer, options.refreshInterval);

            function updateTimer() {
                    value += increment;
                    loopCount++;
                    $(_this).html(value.toFixed(options.decimals).replace(/\B(?=(\d{3})+(?!\d))/g, ","));

                    if (typeof(options.onUpdate) == 'function') {
                        options.onUpdate.call(_this, value);
                    }

                    if (loopCount >= loops) {
                        clearInterval(interval);
                        value = options.to;

                        if (typeof(options.onComplete) == 'function') {
                            options.onComplete.call(_this, value);
                        }
                    }
                }
        });
    };

    $.fn.countTo.defaults = {
        from: 0,  // the number the element should start at
        to: 100,  // the number the element should end at
        speed: 1000,  // how long it should take to count between the target numbers
        refreshInterval: 100,  // how often the element should be updated
        decimals: 0,  // the number of decimal places to show
        onUpdate: null,  // callback method for every time the element is updated,
        onComplete: null,  // callback method for when the element finishes updating
    };
})(jQuery);