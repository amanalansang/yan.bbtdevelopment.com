$(document).ready( function(){
    $(window).bind('scroll', function () {
        var stickyHeaderTop = $('#page-form-banner').offset().top;

           if ($(window).scrollTop() > stickyHeaderTop + 30) {
            $('div.gf_page_steps').addClass('scrolling');
        } else {
            $('div.gf_page_steps').removeClass('scrolling');                  
        }
    })

    function addFinishNa(){
        var ee = '<div id="gf_step_7_x" class="gf_step gf_step_finish gf_step_pending"><span class="gf_step_number">4</span>&nbsp;<span class="gf_step_label">Finish</span></div>'
        $(ee).insertBefore('.gf_page_steps .gf_step_clear');
    }
    addFinishNa();
    /*$('input[name=input_27]').change(function(){
        var secondStep = $('.gf_page_steps div').eq(1);
        secondStep.find('.gf_step_label').text($(this).val()) ;
    })*/

    $( ".frm-application_wrapper" ).on( "change","input[name=input_27]", function() {
        var secondStep = $('.gf_page_steps div').eq(1);
        secondStep.find('.gf_step_label').text($(this).val()) ;
    });
    
    $(window).load(function(){
        var secondStep = $('.gf_page_steps div').eq(1);
        secondStep.find('.gf_step_label').text($('input[name=input_27]:checked').val()) ;               
    });    

})

    jQuery(document).bind('gform_page_loaded', function(event, form_id, current_page){
        addFinishNa();

        $('input[name=input_27]').each(function(){
            var attr = $(this).attr('checked') ;
            if (typeof attr !== typeof undefined && attr !== false) {
                var secondStep = $('.gf_page_steps div').eq(1);
                secondStep.find('.gf_step_label').text($(this).val()) ;
        }

        })
       
           var secondStep = $('.gf_page_steps div').eq(1);
        secondStep.find('.gf_step_label').text($('input[name=input_27]:checked').val()) ;  
    });
