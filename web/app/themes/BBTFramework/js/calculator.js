jQuery( document ).ready( function( $ ) {
  limitNumber();

  let i_loan_amount = $( ".borrowing-amount" );
  let i_interest_rate = $( ".interest-rate" );
  let i_term_length = $( ".year-term" );
  let i_repayment_amount = $( ".monthly-fee" );
  let i_frequency = $( ".frequency-option" );
  const i_extra_repayment_amount = $( ".repayment-amount" );

  const l_repayments_label = $( ".repayments_label" );
  const o_weekly_repayments = $( ".weeklyrepayments" );
  const o_total_interest = $( ".totalinterest" );
  const o_total_payment = $( ".totalamountpayment" );
  const o_total_interest_saved = $( ".principalrepaidtotal" );
  const o_duration = $( ".duration" );
  const o_loan_times_value = $( ".loantimesvalue" );

  const compound_period = 12;
  let minimum_repayment;

  let f_term_length_val; let
    f_interests_val;

  // as soon as the loan amount, annual interest rate and term length fields are filled in calculate the rest
  i_loan_amount.on( "change paste keyup", function() {
    this.value = this.value.replace( /[^0-9.,]/g, "" );

    // skip for arrow keys
    if ( event.which >= 37 && event.which <= 40 ) {
return;
}

    // format number
    $( this ).val( ( index, value ) => value
      .replace( /\D/g, "" )
      .replace( /\B(?=(\d{3})+(?!\d))/g, "," ) );

    const input = $( this ).val();
    const convert_input = cmc_numberWithoutCommas( input );
    $( this ).attr( "data-value", convert_input );

    if ( $( this ).val() !== "" ) {
      continueform();
      maybeInitCalc();
      enablebutton();
    } else {
      fillform();
      hideresult();
      disablebutton();
    }
  } );

  i_interest_rate.on( "change paste keyup", function() {
    this.value = this.value.replace( /[^0-9.]/g, "" );
    if ( $( this ).val() !== "" ) {
      continueform();
      maybeInitCalc();
      enablebutton();
    } else {
      fillform();
      hideresult();
      disablebutton();
    }
  } );

  i_term_length.on( "change paste keyup", function() {
    this.value = this.value.replace( /[^0-9]/g, "" );
    if ( $( this ).val() !== "" ) {
      continueform();
      enablebutton();
      f_term_length_val = "";
      maybeInitCalc();
      o_duration.text( suffixText( $( this ).val(), "years" ) );
    } else {
      fillform();
      hideresult();
      disablebutton();
    }
  } );

  i_repayment_amount.on( "change paste keyup", function() {
    this.value = this.value.replace( /[^0-9,.]/g, "" );

    // skip for arrow keys
    if ( event.which >= 37 && event.which <= 40 ) {
      return;
    }

    // format number
    $( this ).val( ( index, value ) => value
      .replace( /\D/g, "" )
      .replace( /\B(?=(\d{3})+(?!\d))/g, "," ) );

    const input = $( this ).val();
    const convert_input = cmc_numberWithoutCommas( input );
    $( this ).attr( "data-value", convert_input );
    if ( $( this ).val() !== "" ) {
      continueform();
      solveForLoanTerm();
      enablebutton();
    } else {
      fillform();
      hideresult();
      disablebutton();
    }
  } );

  i_repayment_amount.on( "change paste keyup", function() {

    // skip for arrow keys
    if ( event.which >= 37 && event.which <= 40 ) {
      return;
    }

    // format number
    $( this ).val( ( index, value ) => value
      .replace( /\D/g, "" )
      .replace( /\B(?=(\d{3})+(?!\d))/g, "," ) );

    const input = $( this ).val();
    const convert_input = cmc_numberWithoutCommas( input );
    $( this ).attr( "data-value", convert_input );

    if ( $( this ).val() !== "" ) {
      continueform();
      enablebutton();
      const val = cmc_numberWithoutCommas( $( this ).val() );
      if ( minimum_repayment !== 0 && val < minimum_repayment ) {
        setHelperLabel( i_repayment_amount, "Repayment is too low" );
        sweepEverything();
      } else {
        cleanHelperLabels();
        fillform();
        hideresult();
        disablebutton();
      }
    }
  } );

  i_frequency.on( "change", function() {
    l_repayments_label.text( suffixText( this.options[ this.selectedIndex ].text, "REPAYMENTS" ) );
    maybeInitCalc();
  } ).change();

  i_extra_repayment_amount.on( "change paste keyup", function() {

    // skip for arrow keys
    if ( event.which >= 37 && event.which <= 40 ) {
      return;
    }

    // format number
    $( this ).val( ( index, value ) => value
      .replace( /\D/g, "" )
      .replace( /\B(?=(\d{3})+(?!\d))/g, "," ) );

    const input = $( this ).val();
    const convert_input = cmc_numberWithoutCommas( input );
    $( this ).attr( "data-value", convert_input );
    //console.log('exrea payment value = ' + convert_input);

    continueform();
    enablebutton();
    this.value = this.value.replace( /[^0-9,]/g, "" );
    if ( !empty( convert_input ) ) {
      solveForLoanTermWithExtraRepayment( true );
    } else {
      maybeInitCalc();
      fillform();
      disablebutton();
      o_total_interest_saved.text( "-" );
    }
  } );

  /**
   * Can be used when repayment amount is static
   */
  function solveForLoanTermWithExtraRepayment( displayed_saved = false ) {
    const repayamt = cmc_numberWithoutCommas( i_repayment_amount.val() );
    let original_repayamt = repayamt;
    let loanamt = cmc_numberWithoutCommas( i_loan_amount.val() );
    let rate = cmc_numberWithoutCommas( i_interest_rate.val() );
    const paymentfreq = i_frequency.val();
    const balloonpay = 0;
    const extra_repayment = cmc_numberWithoutCommas( i_extra_repayment_amount.val() );
    //console.log('exrta payment - ' + extra_repayment);

    let nper = 0;
    let interest;
    const startingloanamt = loanamt;
    rate = ( rate / 100 ) / paymentfreq;
    let flag = 0;

    //let additional_payment_added = false;

    while ( loanamt > balloonpay ) {
      interest = loanamt * rate;
      loanamt += interest;
      loanamt -= (repayamt + extra_repayment);

      // if ( !empty( extra_repayment ) && additional_payment_added === false ) {
      //   loanamt -= extra_repayment;
      //   additional_payment_added = true;
      // }

      if ( loanamt > startingloanamt ) {
        flag = 1;
        break;
      }
      nper++;
    }
    if ( flag === 1 ) {

      //      $('#loanterm-'+option).val(30);
      //      $('#loantermrange-'+option).val(30);
      //      $('#loanterm-'+option).trigger("change");
      //      solveForRepaymentAmount(option);
    } else {
      let loanterm = nper / paymentfreq;
      loanterm = Math.round( loanterm * 100 ) / 100;
      //console.log('loan team  - ' + loanterm);

      f_term_length_val = loanterm;

      i_term_length.val( Math.ceil( loanterm ) );

      calcAndDisplayTotals( original_repayamt + extra_repayment, loanterm, displayed_saved );
    }
  }

  /**
   * Can be used when repayment amount is static
   */
  function solveForLoanTerm( displayed_saved = false ) {
    const repayamt = cmc_numberWithoutCommas( i_repayment_amount.val() );
    let loanamt = cmc_numberWithoutCommas( i_loan_amount.val() );
    let rate = cmc_numberWithoutCommas( i_interest_rate.val() );
    const paymentfreq = i_frequency.val();
    const balloonpay = 0;

    let nper = 0;
    let interest;
    const startingloanamt = loanamt;
    rate = ( rate / 100 ) / paymentfreq;
    let flag = 0;

    //let additional_payment_added = false;

    while ( loanamt > balloonpay ) {
      interest = loanamt * rate;
      loanamt += interest;
      loanamt -= repayamt;

      // if ( !empty( extra_repayment ) && additional_payment_added === false ) {
      //   loanamt -= extra_repayment;
      //   additional_payment_added = true;
      // }

      if ( loanamt > startingloanamt ) {
        flag = 1;
        break;
      }
      nper++;
    }
    if ( flag === 1 ) {

      //      $('#loanterm-'+option).val(30);
      //      $('#loantermrange-'+option).val(30);
      //      $('#loanterm-'+option).trigger("change");
      //      solveForRepaymentAmount(option);
    } else {
      let loanterm = nper / paymentfreq;
      loanterm = Math.round( loanterm * 100 ) / 100;
      f_term_length_val = loanterm;

      i_term_length.val( Math.ceil( loanterm ) );

      calcAndDisplayTotals( repayamt, loanterm, displayed_saved );
    }
  }

  function calcAndDisplayTotals( repayments, term, displayed_saved = false ) {
    showresult();
    //console.log('from calcAndDisplayTotals');
    o_weekly_repayments.text( beaufityCurrency( repayments ) );
    o_duration.text( suffixText( Math.ceil( term ), "years" ) );

    const i_l_val = cmc_numberWithoutCommas( i_loan_amount.val() );

    const i_frequency_val = i_frequency.val();

    const no_of_payments = i_frequency_val * term;
    //console.log('term' + term);
    //console.log('no of payments - ' + no_of_payments);
    //console.log('repayments - ' + repayments);

    const total_to_pay = no_of_payments * repayments;
    o_total_payment.text( beaufityCurrency( total_to_pay.toFixed( 2 ) ) );

    const total_interest = total_to_pay - i_l_val;


    const original_interest = f_interests_val;

    f_interests_val = total_interest;

    if ( Number( original_interest ) > Number( total_interest ) && displayed_saved ) {
      const interest_saved = original_interest - total_interest;
      o_total_interest_saved.text( beaufityCurrency( interest_saved ) );
    }

    o_total_interest.text( beaufityCurrency( total_interest.toFixed( 2 ) ) );


    const interest_times = ( total_to_pay / i_l_val );
    o_loan_times_value.text( suffixText( interest_times.toFixed( 1 ), "x" ) );
  }

  function sweepEverything() {
    i_term_length.val( "-" );
    o_weekly_repayments.text( "-" );
    o_total_interest.text( "-" );
    o_total_payment.text( "-" );
    o_duration.text( "-" );
    o_loan_times_value.text( "-" );
    o_total_interest_saved.text( "-" );
  }

  function cleanHelperLabels() {
    $.each( $( "span.helper_label" ), function( elem ) {
      $( this ).text( "" );
    } );
  }

  function maybeInitCalc() {
    //console.log( "Running maybeInitCalc" );

    const i_l_val = cmc_numberWithoutCommas( i_loan_amount.val() );
    const i_ir_val = cmc_numberWithoutCommas( i_interest_rate.val() );

    const i_tl_val = f_term_length_val === "" || f_term_length_val === 0 || f_term_length_val === undefined ? cmc_numberWithoutCommas( i_term_length.val() ) : f_term_length_val;
    const i_frequency_val = i_frequency.val();

    // if either of required field are empty, bail out!
    if ( empty( i_l_val ) || empty( i_ir_val ) || empty( i_tl_val ) ) {
      return false;
    }

    // get the monthly interest rate
    const atomic_interest = ( Math.pow( ( 1 + ( i_ir_val / 100 ) / compound_period ), ( compound_period / i_frequency_val ) ) - 1 ) * 100;

    //console.log( `term length value - ${i_tl_val}` );

    // this is the recommended repayments
    let repayments = pmt( ( atomic_interest / 100 ), i_tl_val * i_frequency_val, i_l_val, 0, 0 );
    minimum_repayment = repayments;
    repayments = Math.abs( repayments );
    //console.log( `repayments calculated - ${repayments}` );
    //console.log( `minimum payment set to - ${minimum_repayment}` );

    i_repayment_amount.val( beaufityCurrency( repayments, false ) );

    // display weekly/monthly/fortnightly payments
    o_weekly_repayments.text( beaufityCurrency( repayments ) );

    calcAndDisplayTotals( repayments, i_tl_val );
  }

  function pmt( rate_per_period, number_of_payments, present_value, future_value, type ) {
    if ( rate_per_period !== 0.0 ) {

      // Interest rate exists
      const q = Math.pow( 1 + rate_per_period, number_of_payments );
      return -( rate_per_period * ( future_value + ( q * present_value ) ) ) / ( ( -1 + q ) * ( 1 + rate_per_period * ( type ) ) );
    } if ( number_of_payments !== 0.0 ) {

      // No interest rate, but number of payments exists
      return -( future_value + present_value ) / number_of_payments;
    }

    return 0;
  }

  function empty( val ) {

    // test results
    //---------------
    // []        true, empty array
    // {}        true, empty object
    // null      true
    // undefined true
    // ""        true, empty string
    // ''        true, empty string
    // 0         false, number
    // true      false, boolean
    // false     false, boolean
    // Date      false
    // function  false

    if ( val === undefined ) {
      return true;
    }

    if ( typeof( val ) === "function" || typeof( val ) === "boolean" || Object.prototype.toString.call( val ) === "[object Date]" ) {
     return false;
    }

    if ( val == null || val.length === 0 ) // null or 0 length array
    {
     return true;
    }

    if ( val === 0 ) {
      return true;
    }

    if ( typeof( val ) === "object" ) {

      // empty object

      let r = true;

      for ( const f in val ) {
       r = false;
      }

      return r;
    }

    return false;
  }

  function setHelperLabel( elem, text ) {
    elem.closest( ".col-12" ).find( "span.helper_label" ).text( text );
  }


  function beaufityCurrency( string, prefix_dollar = true ) {
    const final_str = addCommas( Math.floor( string ) );
    if ( prefix_dollar ) {
return `$${final_str}`;
}

    return final_str;
  }

  function suffixText( string, suffix ) {
    return `${string} ${suffix}`;
  }

  function cmc_numberWithoutCommas( val ) {
    val = val.toString();
    return Number( val.replace( /[^0-9\.]+/g, "" ) );
  }

  function addCommas( nStr ) {
    nStr += "";
    x = nStr.split( "." );
    x1 = x[ 0 ];
    x2 = x.length > 1 ? `.${x[ 1 ]}` : "";
    const rgx = /(\d+)(\d{3})/;
    while ( rgx.test( x1 ) ) {
      x1 = x1.replace( rgx, "$1" + "," + "$2" );
    }
    return x1 + x2;
  }

  $( ".mortgage-bottom a.back-button" ).click( ( event ) => {

    // event.preventDefault();
    /* Act on the event */
    hideresultMobile();
  } );
  function continueform() {
    $( ".no-result h4" ).text( "Continue the form to calculate how much you could be paying" );
  }
  function fillform() {
    $( ".no-result h4" ).text( "Fill in the form to calculate how much you could be paying" );
  }
  function showresult() {
    $( ".no-result" ).hide();
  }
  function hideresult() {
    $( ".no-result" ).show();
  }
  function enablebutton() {
    $( ".calculator_form .result-calculate a" ).removeClass( "disable" );
    $( ".calculator_form .result-calculate a" ).click( ( event ) => {

      // event.preventDefault();
      /* Act on the event */
      showresultMobile();
    } );
  }
  function disablebutton() {
    $( ".calculator_form .result-calculate a" ).addClass( "disable" );
  }
  function showresultMobile() {
    $( "#MortCalc1.mort-wrap .mortgage-bottom" ).show();
    $( "#MortCalc1.mort-wrap .mortgage-top .col-md-4" ).hide();
  }
  function hideresultMobile() {
    $( "#MortCalc1.mort-wrap .mortgage-bottom" ).hide();
    $( "#MortCalc1.mort-wrap .mortgage-top .col-md-4" ).show();
  }
  function limitNumber() {
  	$( "#MortCalc1 .calculator_form input[type=text]" ).each( function( index, el ) {
		  		$( this ).on( "change paste keyup", function( event ) {

  		  			// if (input > 0) {
  		  			// 	if (input < 9999999.99) {
  				  	// 		if (input == Math.floor(input)) {
  			  		// 			//console.log('integer')

  				  	// 		} else {
  							//     var num = parseFloat(input);
  							//     var cleanNum = num.toFixed(2);
  							//     $(this).val(cleanNum);
  							//     if(num/cleanNum < 1){
  							//         return false;
  							//       }
  				  	// 		}
  		  			// 	} else {
  		  			// 		$(this).val(9999999.99);
  		  			// 	}
  		  			// }

        //console.log( $( this ).val() );
			    } );
  	} );
  }
} );
