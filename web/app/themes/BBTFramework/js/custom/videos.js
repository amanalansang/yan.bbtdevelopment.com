jQuery(document).ready(function ($) {
    "use strict";

    const blackScreen = $("#black-screen");
    const videoHaven = $("#video-haven");
    const videoDep = $("#video-deployed");
    const videoHavenContainer = $(".video-haven");
    const videoDepContainer = $(".video-deployed");

    /** start of video popups */
    function stopVideo(video) {
        video.get(0).pause();
        video.get(0).currentTime = 0;
    }

    //homepage
    $(".play-me").click(function () {
        if (videoDep.length) {
            videoDep.get(0).play();
        }
        blackScreen.css("display", "block");
        videoDepContainer.css("display", "block");
    });

    //other pages
    $(".play-this").each(function () {
        $(this).click(function () {
            if (videoHaven.length) {
                videoHaven.get(0).play();
            }
            blackScreen.css("display", "block");
            videoHavenContainer.removeClass("d-none").addClass("d-block");
        });
    });

    $(".stop-this").each(function () {
        $(this).click(function () {
            if (videoHaven.length) {
                stopVideo(videoHaven);
            }
            if (videoDep.length) {
                stopVideo(videoDep);
            }
            blackScreen.css("display", "none");
            videoHavenContainer.removeClass("d-block").addClass("d-none");
            videoDepContainer.css("display", "none");
        });
    });

    $(document).keyup(function(e) {
        if (e.key === "Escape") { // escape key maps to keycode `27`
            if (videoHaven.length) {
                stopVideo(videoHaven);
            }
            if (videoDep.length) {
                stopVideo(videoDep);
            }
            blackScreen.css("display", "none");
            videoHavenContainer.removeClass("d-block").addClass("d-none");
            videoDepContainer.css("display", "none");
        }
    });

    $("#black-screen").click(function () {
        var thisBlackScreen = $(this);

        if (videoHaven.length) {
            stopVideo(videoHaven);
        }
        if (videoDep.length) {
            stopVideo(videoDep);
        }
        if (videoHavenContainer.length) {
            videoHavenContainer.removeClass("d-block").addClass("d-none");
        }
        if (videoDepContainer.length) {
            videoDepContainer.css("display", "none");
        }
        thisBlackScreen.css("display", "none");
    });
    /** end of videos*/

});