jQuery(document).ready(function($) {
    var TABLET_PORTRAIT = 768;

	$('#header').wrapInner("<div class='header-inner haven-container'></div>");
    
    $(window).on('load', function(){
    
		$('.benefits-questions .wpb_content_element a').each(function(){
			var link = $(this).attr('href');
			$(this).closest('p').wrap('<a href="'+link+'"></a>');
		});

		$('#footer').wrapInner("<div class='footer-inner'></div>");

		$('.haven-breadcrumbs .divider').append("<img src='/app/uploads/2017/08/breadcrumb.png'>");

		$('.ks-blue-arrow .vc_icon_element-inner').html('<img src="/app/uploads/2017/08/arrow-blue-down.png">');

		$('.read-assumptions p').toggle(function(){
			$('.assumptions').slideDown();
            $(this).addClass('active');
            $(this).text('Assumptions');
		}, function(){
			$('.assumptions').slideUp();
            $(this).text('Click to read assumptions');
            $(this).removeClass('active');
		});

		$('.right-plan-yn').click(function(){
			$(this).addClass('choose-me');
			$(this).removeClass('unchoose-me');
			$(this).siblings().addClass('unchoose-me');
			$(this).siblings().removeClass('choose-me');
		});

		$(function() {
		  $('.page-mortgages .top-container-menu a, .page-kiwisaver .top-container-menu a, .page-insurance .top-container-menu a, .page-accounting .top-container-menu a, .ourPackages a').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		      var target = $(this.hash);
		      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		      if (target.length) {
		        $('html, body').animate({
		          scrollTop: target.offset().top
		        }, 1000);
		        return false;
		      }
		    }
		  });
		});

        $('.b-f .list .option').wrapInner('<span></span>');

        var blog_home_title = '';
        var blog_home_title_trim = '';

        $('.blog-home a').each(function(){
            blog_home_title = $(this).text();
            if (blog_home_title.length > 40) {
                blog_home_title_trim = blog_home_title.trim().substring(0, 40).split(" ").join(" ") + "...";
                $(this).text(blog_home_title_trim);
            }
        });

        $('.single-post #menu-item-1678').addClass('current-menu-parent');

        $(".popup__inner").prepend("<span class='close'>x</span>");

        $(".popup__inner .close").click(function() {
            $(".popup.main").fadeOut();
        });

        $(".popup__btn").click(function () {
            $(".popup.main").fadeIn();
        });

        function getCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
            }
            return null;
        }

        setTimeout(function() {
            // var url_string = window.location.href;
            // var url = new URL(url_string);
            // var dl = url.searchParams.get("dl");
            var popup = getCookie("popup");
            if(popup != "true") {
                $(".popup.main").fadeIn();
            }
        }, 2000);

        setInterval(function(){
            $('input').focus(function(){
                $(this).parent().parent().find('.validation_message').fadeOut();
            });
        },500);
	
	});

    $(document).on('gform_confirmation_loaded', function(event, formId){
        var d = new Date();
        d.setTime(d.getTime() + (99999*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = "popup=true;" + expires + ";path=/";
    });

	$(window).on('load', function() {
        $('.home-tools h2').equalizeHeights();
		$('#desktop-slider-list .benefits-text').equalizeHeights();
        $('.other-services .benefits-text').equalizeHeights();
        $('.kiwisaver-benefits .benefits-text').equalizeHeights();
        $('.havenBenefits > div .wpb_single_image').equalizeHeights();
		// $('.ks-slide-text').equalizeHeights();
        $('.other-services h4').equalizeHeights();
        $('.o-s h2').equalizeHeights();
        $('.acc-accounting .ks-m-top p').equalizeHeights();
        if ($(window).width() >= 752) {
            setTimeout(function(){
                $('.service-img').equalizeHeights();
                $('.acc-accounting .wpb_single_image').equalizeHeights();
            },500);
        }


	});

    $(window).resize(function(){
        $('#desktop-slider-list .benefits-text, .other-services .benefits-text, .kiwisaver-benefits .benefits-text, .ks-slide-text, .other-services h4, .home-tools h2, .service-img, .o-s h2, .acc-accounting .ks-m-top p, .acc-accounting .wpb_single_image, .havenBenefits > div .wpb_single_image').css('height', 'auto');
        setTimeout(function(){
            $('.o-s h2').equalizeHeights();
            $('.service-img').equalizeHeights();
            $('.home-tools h2').equalizeHeights();
            $('#desktop-slider-list .benefits-text').equalizeHeights();
            $('.other-services .benefits-text').equalizeHeights();
            $('.kiwisaver-benefits .benefits-text').equalizeHeights();
            // $('.ks-slide-text').equalizeHeights();
            $('.other-services h4').equalizeHeights();
            $('.acc-accounting .ks-m-top p').equalizeHeights();
            $('.havenBenefits > div .wpb_single_image').equalizeHeights();
        },100);
        if ($(window).width() >= 752) {
            $('.acc-accounting .wpb_single_image').equalizeHeights();
        }
    });

	$.fn.equalizeHeights = function(){
		return this.height( Math.max.apply(this, $(this).map(function(i,e){ return $(e).height() }).get() ) )
	}

    function mortgageList(){
        if($(window).width() <= TABLET_PORTRAIT) {
            $( '#mortgageList' ).tabs().removeClass( 'ui-tabs-vertical ui-helper-clearfix' );
            $( '#mortgageList li' ).addClass( 'ui-corner-top' ).removeClass( 'ui-corner-left' );
            var mortgageListHeadWidth = 0;
            $('.m-l-tabs-header ul li').each(function(){
                mortgageListHeadWidth += $(this).outerWidth(true);
                console.log(mortgageListHeadWidth);
                $(this).parent().css('width',mortgageListHeadWidth + 10);
            });
        } else {
            $( '#mortgageList' ).tabs().addClass( 'ui-tabs-vertical ui-helper-clearfix' );
            $( '#mortgageList li' ).removeClass( 'ui-corner-top' ).addClass( 'ui-corner-left' );
            $('.m-l-tabs-header ul').css('width', 'auto');
        }
    }
    $(window).resize(function(){
        mortgageList();
    });

    mortgageList();

    var currentTab = 0;

    // $('#mortgageList').click(function(){
    //     setTimeout(function(){
    //         var prev_text = $('.ui-tabs-active').prev().find('a').clone().children().remove().end().text();
    //         var next_text = $('.ui-tabs-active').next().find('a').clone().children().remove().end().text();
    //         if(next_text == ''){
    //             $("#btnMoveRightTab").hide();
    //         } else if(prev_text == '') {
    //             $("#btnMoveLeftTab").hide();
    //         } else {
    //             $("#btnMoveRightTab").show()
    //             $("#btnMoveLeftTab").show()
    //         }
    //         $("#btnMoveRightTab").text(next_text);
    //         $("#btnMoveLeftTab").text(prev_text);
    //     },100);
    // });

    var $tabs = $('#mortgageList').tabs();
    
    $(".ui-tabs-panel").each(function(i){
      var totalSize = $(".ui-tabs-panel").size() - 1;
      var prevName = $(this).prev().find('.d-t').text();
      var nextName = $(this).next().find('.d-t').text();
      if (i != totalSize) {
          next = i + 1;
          $(this).find('.m-l-tabs-pn').append("<a href='#' class='next-tab mover' rel='" + next + "'>" + nextName + "</a>");
      }
      if (i != 0) {
          prev = i - 1;
          $(this).find('.m-l-tabs-pn').append("<a href='#' class='prev-tab mover' rel='" + prev + "'>" + prevName + "</a>");
      }
    });

    $('.next-tab, .prev-tab').click(function() { 
       $tabs.tabs('option', 'active', $(this).attr("rel"));
       return false;
    });

    $('#mortgageList a').click(function(){
        add_mort();
    });

    $('.y-v input').change(function(){
        add_mort();
    });

    add_mort();

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function add_mort() {
        var average_arr = [];
        var a_average_arr = [];
        var y_v_arr = [];
        var count = 0;
        var ave_text = '';
        var a_ave_text = '';
        $('.m-l-tabs > div').each(function(){
            var average = 0;
            var a_average = 0;
            var t_average = 0;
            var t_a_average = 0;
            var y_v = 0
            var t_y_v = 0
            $(this).find('.row').each(function(){
                average = $(this).find('.average span.calc_val').text();
                a_average = $(this).find('.a-average span.calc_val').text();
                y_v = $(this).find('input').val();
                y_v = Number(y_v) || 0;
                a_average = Number(a_average);
                average = Number(average);
                t_average += average;
                t_a_average += a_average;
                t_y_v += y_v;

                ave_text = numberWithCommas(average);
                a_ave_text = numberWithCommas(a_average);
                $(this).find('.average').append('<span class="calc_text">' + ave_text + '</span>');
                $(this).find('.a-average').append('<span class="calc_text">' + a_ave_text + '</span>');

                t_average_text = numberWithCommas(t_average);
                t_a_average_text = numberWithCommas(t_a_average);
                t_y_v_text = numberWithCommas(t_y_v);
                $(this).find('.t-average').text(t_average_text);
                $(this).find('.t-a-average').text(t_a_average_text);
                $(this).find('.t-y-v').text(t_y_v_text);
            });
            var average_input = $(this).find('.t-average').text().replace(/,/g, '');
            average_input = Number(average_input);
            average_arr.push(average_input);
            var a_average_input = $(this).find('.t-a-average').text().replace(/,/g, '');
            a_average_input = Number(a_average_input);
            a_average_arr.push(a_average_input);
            var y_v_input = $(this).find('.t-y-v').text().replace(/,/g, '');
            y_v_input = Number(y_v_input);
            y_v_arr.push(y_v_input);
        });
        $('#contenttotal .l-t-b .row').each(function(){
            $(this).find('.average span.calc_val').text(average_arr[count]);
            $(this).find('.a-average span.calc_val').text(a_average_arr[count]);

            ave_text = numberWithCommas(average_arr[count]);
            a_ave_text = numberWithCommas(a_average_arr[count]);
            $(this).find('.average').append('<span class="calc_text">' + ave_text + '</span>');
            $(this).find('.a-average').append('<span class="calc_text">' + a_ave_text + '</span>');

            $(this).find('.y-v input').val(y_v_arr[count]);
            count++;
        });

    }

    $('input.range-slide').change(function(){
        var rangeSlide = $(this).val().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this).closest('.row').find('span.range-slide').text(rangeSlide);
    });


    var count = 1;
    $('.mort-wrap').each(function(){
        $(this).find('input[type=radio]').attr('name', 'frequency'+count);
        $(this).find('#f1').attr('id','f1-'+count).siblings('label').attr('for','f1-'+count);
        $(this).find('#f2').attr('id','f2-'+count).siblings('label').attr('for','f2-'+count);
        $(this).attr('id', 'MortCalc'+ count);
        count++;
    });

    $('.b-f form').on('submit', function(e){
        e.preventDefault();
        var link = $(this).serialize().replace('input_1=','').replace('%2F','/');
        var redirect_link = link.substring(0, link.lastIndexOf('&is_submit') + 1).replace('&','');
        window.location.href = redirect_link;
    });

    $('.progress-bar').each(function(){
        $(this).prepend('<progress value="0" max="0"></progress>');
    });

    function progressBar() {
        var progressPercentage = 0;
        $('.progress-bar').each(function(){
            var progressBarVal = $(this).find('.p-b-val').text().replace('$','').replace(/,/g,'');
            var progressBarMax = $(this).find('.p-b-max').text().replace('$','').replace(/,/g,'');
            progressBarVal = Number(progressBarVal);
            progressBarMax = Number(progressBarMax);
            var pbvalWidth = $(this).find('.p-b-val').width();
            progressPercentage = ((progressBarVal / progressBarMax) * 100);
            $(this).find('.p-b-val').css({
                'left': progressPercentage + '%',
                'margin-left': '-' + pbvalWidth/2 + 'px'
            });
            $(this).find('progress').attr({'value':progressBarVal,'max':progressBarMax});
        });
    }

    setTimeout(function(){
        $('.t-g .vc_grid-item').first().addClass('current-testi');
        $('<img src="/app/themes/BBTFramework/images/quotation.png" class="quotation" />').insertBefore('.t-c');
    },2000);

    $('#main-nav > li').hover(function(){
        $(this).children('.sub-menu').toggleClass('active-sub');
    });

    $('#main-nav .sub-menu li.menu-item-has-children').each(function(){
        $(this).find('> a').append('<i class="material-icons">keyboard_arrow_right</i>');
    });

    $(window).scroll(function(){
        var topDistance = $(document).scrollTop();
        if(topDistance >= 1){
            $('.active-sub').addClass('fixed-sub');
            $('.header-wrap #header').addClass('scrolling').slideDown();
        }else if(topDistance == 0){
            $('.sub-menu').removeClass('fixed-sub');
            $('.header-wrap #header').removeClass('scrolling');
        }

        // if($(window).width() >= 941) {
        //     if(topDistance >= 280) {
        //         $('.top-container-menu').addClass('scrolling').slideDown();
        //     }else if(topDistance <= 279) {
        //         $('.top-container-menu').removeClass('scrolling');
        //     }

        //     $('.target').each(function() {
        //         var sec_row = $(this).offset().top-61;
        //         if($(window).scrollTop() >= sec_row) {
        //             var id = $(this).attr('id');
        //             $('.top-container-menu a').removeClass('active');
        //             $('.top-container-menu a[href=#'+ id +']').addClass('active');
        //         }
        //     });
        // } else {
        //     $('.top-container-menu').removeClass('scrolling');
        // }

    });

    $('.ks-slider > .wpb_column  > .vc_column-inner > .wpb_wrapper').wrapInner('<ul class="ks-work-slider"></ul>');
    $('.ks-slide').wrap('<li></li>');
    var slider = $('.ks-slider.desktop .ks-work-slider').bxSlider({
        slideWidth: 930,
        infiniteLoop: false, 
        hideControlOnEnd: true,  
        adaptiveHeight: true,
        adaptiveHeightSpeed: 100,
        onSliderLoad: function() {
            $(".ks-work-slider li:not([class='bx-clone'])").eq(0).addClass('current');
            $(".ks-work-slider li").find('progress').attr({value:0,max:0});
            progressBar();
        },
        onSlideBefore: function() {
            $(".ks-work-slider li").removeClass('current');
            $(".ks-work-slider li").find('progress').attr({value:0,max:0});
            current = slider.getCurrentSlide();
            $(".ks-work-slider li:not([class='bx-clone'])").eq(current).addClass('current');
        },
        onSlideAfter: function() {
            setTimeout(function(){
                progressBar();
            },500);
        }
    });

    var slider2 = $('.ks-slider.mobile .ks-work-slider').bxSlider({
        slideWidth: 930,
        infiniteLoop: false, 
        hideControlOnEnd: true, 
        adaptiveHeight: true,
        adaptiveHeightSpeed: 100,
        onSliderLoad: function() {
            $(".ks-work-slider li:not([class='bx-clone'])").eq(0).addClass('current');
            $(".ks-work-slider li").find('progress').attr({value:0,max:0});
            progressBar();
        },
        onSlideBefore: function() {
            $(".ks-work-slider li").removeClass('current');
            $(".ks-work-slider li").find('progress').attr({value:0,max:0});
            current = slider2.getCurrentSlide();
            $(".ks-work-slider li:not([class='bx-clone'])").eq(current).addClass('current');
        },
        onSlideAfter: function() {
            setTimeout(function(){
                progressBar();
            },500);
        }
    });

    // $('.mobile-slider').find('> div').wrap('<li></li>');
    // $('.mobile-slider').wrapInner('<ul class="mobile-slider-list"></ul>');
    $('.mobile-slider, .mobile-slider-rows').attr('id', 'desktop-slider-list').clone().insertAfter('.mobile-slider, .mobile-slider-rows').attr('id', 'mobile-slider-list');
    $('#mobile-slider-list.mobile-slider').wrapInner('<ul class="carousel"></ul>');
    $('#mobile-slider-list.mobile-slider > ul.carousel').find('> div').wrap('<li></li>');
    $('#mobile-slider-list.mobile-slider-rows > .vc_column-inner > .wpb_wrapper').wrapInner('<ul class="carousel"></ul>');
    $('#mobile-slider-list.mobile-slider-rows ul.carousel').find('> div').wrap('<li></li>');
    $('ul.carousel').bxSlider({
        slideWidth: 460,
        adaptiveHeight: true,
        adaptiveHeightSpeed: 500
    });   
    

    // $('.t-g').swipe({
    //     swipe:function(event, direction) {
    //         if(direction == 'up'){
    //             $('.t-g .vc_grid-item.current-testi').removeClass('current-testi').next().addClass('current-testi');
    //         }else if(direction == 'down'){
    //             console.log('down');
    //         }
    //     },
    //     threshold:50
    // });

    var tmd = $('.top-container-menu ul').find('li').first().text();
    $('<div class="top-menu-dropdown">' + tmd + '<i class="material-icons">arrow_drop_down</i></div>').insertBefore('.top-container-menu ul');

    function mobileTopMenu(){
        if($(window).width() <= TABLET_PORTRAIT) {
            $('.top-container-menu').addClass('mobile');
        } else {
            $('.top-container-menu').removeClass('mobile');
            $('.top-container-menu ul').show();
        }
    }

    $(window).resize(function(){
        mobileTopMenu();
    });

    mobileTopMenu();

    $(document).on('click', '.top-container-menu.mobile .top-menu-dropdown', function () {
        $(this).parent().find('ul').slideToggle();
        $(this).find('i.material-icons').toggleClass('active');
    });

    $('.our-services').prepend('<span class="service-head"><ul></ul></span>');
    $('.our-services > div').each(function(){
        $(this).attr('id', 'Service'+ count);
        var tabTitle = $(this).find('h2').text();
        $(this).parent().find('.service-head ul').append('<li><a href="#Service'+ count +'">'+ tabTitle +'</a></li>');
        count++;
    });

    function serviceTabs() {
        $('.our-services.mobile').tabs();

        var serviceHeadWidth = 0;
        $('.service-head ul li').each(function(){
            serviceHeadWidth += $(this).outerWidth(true);
            $(this).parent().css('width',serviceHeadWidth + 10);
        });
    }
    if($(window).width() <= TABLET_PORTRAIT) {
        serviceTabs();    
    }
    $(window).resize(function(){
        serviceTabs();
    });

    

    var hpn = $('.haven-page-nav ul.menu').find('li.current_page_item a').first().text();
    $('<div class="top-menu-dropdown">' + hpn + '<i class="material-icons">arrow_drop_down</i></div>').insertBefore('.haven-page-nav ul.menu');
    $('.haven-page-nav ul.menu').find('li.menu-item-has-children > a').append('<i class="material-icons">arrow_drop_down</i>');

    function kiwisaverTopMenu(){
        if($(window).width() <= TABLET_PORTRAIT) {
            $('.haven-page-nav').addClass('mobile');
            $('.haven-page-nav ul.menu > li.menu-item-has-children > a').click(function(e){
                e.preventDefault();
            })
        } else {
            $('.haven-page-nav').removeClass('mobile');
            $('.haven-page-nav ul.menu').show();
        }
    }

    $(window).resize(function(){
        kiwisaverTopMenu();
    });

    kiwisaverTopMenu();

    $(document).on('click', '.haven-page-nav.mobile .top-menu-dropdown', function () {
        $(this).parent().find('ul.menu').slideToggle();
        $(this).find('i.material-icons').toggleClass('active');
    });

    //$('a.post-image').append('<div class="overlay"></div>');
    //$('.post-image a').has('img').append('<div class="overlay"></div>');
    $('.post-image a').has('img').parent().css('background', 'none')
    $('div.post-image').matchHeight();

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    var paramName = getParameterByName('active_tab');

    if(paramName){
        $('.post-search-filter li').each(function(){
            var compareFilter = $(this).attr('id');
            (compareFilter==paramName ? $(this).addClass('active') : $(this).removeClass('active'))
        });
    }

    var activeTab = $('.post-search-filter li.active').attr('id');
    $('.search-grid').each(function(){
        if($(this).hasClass(activeTab)){
            $(this).fadeIn();
        }
    });

    $('.post-social-wrap .share-trig').click(function(){
        console.log('share-trig-test');
        $(this).siblings('.post-social').toggle();
    });

    $('.benefits-questions p > a').each(function() {
        $(this).contents().eq(1).wrap('<span />');
    });

    function fixedFooter() {
        var totalHeight = $('#wrapper').outerHeight();
        var windowHeight = $(window).height();
        var footerHeight = $('footer').outerHeight();

        if(totalHeight<windowHeight) {
            $('.footer-newsletter-inner, footer').addClass('fixed-footer');
            $('.footer-newsletter-inner').css('bottom', footerHeight);
        } else {
            $('.footer-newsletter-inner, footer').removeClass('fixed-footer');
        }
    }

    fixedFooter();
    $(window).resize(function(){
        fixedFooter();
    });

    $('map').tooltip({
        track: true,
        position: { my: "left+15 center", at: "right center",of: "area" }
    });
    $('img[usemap]').rwdImageMaps();

    $('.b-f select').niceSelect();


    $('.testimonial-container__grid ul li').each(function(){
        var index = $(this).index() + 1;
          $(this).find('a').append(index);
    })

});