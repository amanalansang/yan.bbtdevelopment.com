jQuery(document).ready(function ($) {
    "use strict";
    $(".search__header input").attr("placeholder", "Search...");
    $("<span class='searchtrigger'><i class='fa fa-search'></i></span>").insertBefore("#navigation");
    $("#mobile_header h3").prepend("<span class='searchtrigger'><i class='fa fa-search'></i></span>");
    $(".searchtrigger").click(function () {
        $(".search__header").toggleClass("active");
    });
    $(".search__header .close").click(function () {
        $(".search__header").removeClass("active");
    });
});