/*global
$, jQuery, window*/
function unSlickOnDesktop(containerClass) {
    "use strict";
    var container = jQuery(containerClass);

    if(container.length) {
        jQuery(containerClass).slick('unslick');
    }
}

function slickOnMobile(containerClass) {
    "use strict";
    var container = jQuery(containerClass);

    if(container.length) {
        container.not(".slick-initialized").slick({
            dots: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            arrows: false,
            responsive: [
                {
                    breakpoint: 769,
                    settings: "unslick"
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        infinite: true
                    }
                },
            ]
        });
    }
}

jQuery(document).ready(function ($) {
    "use strict";
    var winWidth = jQuery(window).width();

    if(winWidth <= 600) {
        slickOnMobile(".gallery");
    } else {
        unSlickOnDesktop(".gallery");
    }

    function scrollOnSubmit() {
        var headerHeight = $('#header').outerHeight(true);
        if ($(window.width < 992 )){
            headerHeight = $('#mobile_header').outerHeight(true);
        }
        var paddingSpace = 20;

        $('html, body').animate({
            scrollTop: $('.gform_confirmation_message').offset().top - (headerHeight + paddingSpace)
        },2000)

    }

     $(document).bind('gform_confirmation_loaded', function(){
         console.log('ajax')
        scrollOnSubmit();
     });

    $(window).on('load', function() {
        if ($('.gform_confirmation_message').length !== 0) {
            scrollOnSubmit();
        }
    });
});

jQuery(window).on('resize orientationchange', function ($) {
    'use strict';
    var winWidth = jQuery(window).width();

    if(winWidth <= 600) {
        slickOnMobile(".gallery");
    } else {
        unSlickOnDesktop(".gallery");
    }
});