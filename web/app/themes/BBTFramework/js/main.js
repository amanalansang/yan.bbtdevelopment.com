jQuery(document).ready(function($) {

    "use strict";
    if($('.owl-carousel').length) {
        $('.owl-carousel').owlCarousel({
            items: 1,
            merge: true,
            loop: true,
            margin: 10,
            video: true,
            lazyLoad: true,
            center: true,
            nav: true
        });
    }

    var getUrl = window.location;
    const _BASE_URL_ = getUrl.protocol + "//" + getUrl.host;
    const _FILES_URL_ = _BASE_URL_ + "/app/themes/BBTFramework/files/";
    const _DOWNLOAD_URL_ = _BASE_URL_ + "/will-kits/download/";


    function isiPhone(){
        return (
            (navigator.platform.indexOf("iPhone") != -1) ||
            (navigator.platform.indexOf("iPad") != -1) ||
            (navigator.platform.indexOf("iPod") != -1)
        );
    }

    function disclaimer(curHeight, winWidth){
        if( winWidth > 600 ) {
            $('a#disc').on('click', function(e){
                e.preventDefault();
                $('.fancybox-inner').height( curHeight + 300 );
                $('.disclaim').toggleClass('hide');
                if( ! isiPhone ) {
                    $('.fancybox-wrap').addClass('top20').removeClass('topReset');
                    if( $('.disclaim').hasClass('hide') ) {
                        $('.fancybox-wrap').removeClass('top20').addClass('topReset');
                    }
                }
            });
         } else {
            $('.fancybox-inner').css({'max-height': curHeight, 'overflow-y': 'scroll'});
            $('a#disc').on('click', function(e){
                e.preventDefault();
                $('.disclaim').toggleClass('hide');
                console.log('a');
            });
        }
    }

    //will kits
    $('.buttonBlk a.fancyBox').each(function(e) {
        $(this).on('click',function (e) {
            e.preventDefault();

            var $dataCode = $(this).attr('data-code');
            var $target = $(this).attr('href');
            var winHeight = $(window).height();
            var winWidth = $(window).width();

            $.fancybox({
                type        : 'inline',
                href        : $target,
                loop        : false,
                maxWidth    : '650px',
                minHeight   : '400px',
                autoResize  : true,
                scrolling   : false,
                closeClick  : false,
                helpers     : { overlay : {closeClick: false} },
                hideOnContentClick: false,
                closeBtn : true,
                keys : { close: null },
                afterShow   : function(e) {
                    var curHeight = $('.fancybox-inner').outerHeight();

                    $('#label_13_7_1').removeAttr('for');

                    //console.log(winWidth);
                    disclaimer(curHeight);
                },
                afterClose  : function(e) {
                    window.location.reload();
                }
            });

            //reset click functions on ajax submit
            jQuery(document).bind('gform_post_render', function(){

                var curHeight = $('.fancybox-inner').outerHeight();

                $('#label_13_7_1').removeAttr('for');
                //reset popup position to center
                $('.fancybox-wrap').removeClass('top20').addClass('topReset');

                //console.log(winWidth);
                disclaimer(curHeight);

            });
        });
    });


    $(window).load(function(e) {
        loadBannerHeight();
    });

    $(window).bind('orientationchange', function (event) {
    //    location.reload(true);
        loadBannerHeight();
    });

    $(window).resize(function(e) {
        loadBannerHeight();
    });

    $('.owl-theme .owl-dots .owl-dot').click(function(e) {
        $('.owl-carousel video').get(0).play();
    });

    $(window).on('load', function () {

        $('#header').wrapInner("<div class='header-inner haven-container'></div>");
        $('.benefits-questions .wpb_content_element a').each(function(){
            var link = $(this).attr('href');
            $(this).closest('p').wrap('<a href="'+link+'"></a>');
        });

        $('#footer').wrapInner("<div class='footer-inner'></div>");

        $('.haven-breadcrumbs .divider').append("<img src='/app/uploads/2017/08/breadcrumb.png'>");

        $('.ks-blue-arrow .vc_icon_element-inner').html('<img src="/app/uploads/2017/08/arrow-blue-down.png">');

        $('.read-assumptions p').toggle(function(){
            $('.assumptions').slideDown();
        }, function(){
            $('.assumptions').slideUp();
        });

        $('.right-plan-yn').click(function(){
            $(this).addClass('choose-me');
            $(this).removeClass('unchoose-me');
            $(this).siblings().addClass('unchoose-me');
            $(this).siblings().removeClass('choose-me');
        });




        $(function() {
          $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html, body').animate({
                  scrollTop: target.offset().top
                }, 1000);
                return false;
              }
            }
          });
        });

    });

    $(window).on('load resize', function() {
        $('.benefits-text').equalizeHeights();
        $('.ks-slide-text').equalizeHeights();

    });

    $.fn.equalizeHeights = function(){
        return this.height( Math.max.apply(this, $(this).map(function (i,e){ return $(e).height() }).get()));
    }
    setInterval(function(){
        var success = $('#gform_confirmation_wrapper_8').length;
        if(success == 1) {
            $('#success-contact').fadeIn();
            $('#contact-send').fadeOut();
        }
    },500);

    function loadBannerHeight() {
        if( $(window).width() < 980 && $(window).width() > 480 ) {
            var vidHeight = $('.forIpad').height();
        } else if ( $(window).width() < 480 ) {
            var vidHeight = $('.forMobile').height();
        } else {
            var vidHeight = $('.vidImgBanner video').height();
        }
        $('.top-home .bg').css({'height': vidHeight });
    }
});

