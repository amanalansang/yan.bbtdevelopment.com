function unSlickOnDesktop(containerClass) {
    "use strict";
    var container = jQuery(containerClass);

    if(container.length) {
        jQuery(containerClass).slick('unslick');
    }
}

function slickOnMobile(containerClass) {
    "use strict";
    var container = jQuery(containerClass);

    if(container.length) {
        container.not(".slick-initialized").slick({
            centerMode: true,
            slidesToShow: 2,
            dots: true,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            arrows: false,
            mobileFirst: true,
            responsive: [
                {
                    breakpoint: 769,
                    settings: "unslick"
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: false,
                        variableWidth: true
                    }
                },
            ]
        });
    }
}

jQuery(window).on('resize orientationchange', function () {
    'use strict';
    var winWidth = jQuery(window).width();

    if(winWidth <= 812) {
        slickOnMobile(".blog-gallery");
    } else {
        //unSlickOnDesktop(".gallery");
        console.log('cant unslick');
    }
});
