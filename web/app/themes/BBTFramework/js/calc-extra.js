jQuery(document).ready( function() {
	jQuery('.tooltip-icon').each(function(index, el) {
		jQuery(this).click(function(event) {
			/* Act on the event */
			jQuery(this).next('.tooltip-text').fadeToggle()
		});
	});
	jQuery('.close-tooltip').each(function(index, el) {
		jQuery(this).click(function(event) {
			/* Act on the event */
			jQuery(this).parent('.tooltip-text').fadeToggle()
		});
	});
})