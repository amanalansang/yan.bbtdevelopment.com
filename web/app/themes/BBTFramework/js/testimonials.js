/*global
$, jQuery, window*/
jQuery(document).ready(function ($) {
    "use strict";
    starRating();
    jQuery(document).on('gform_post_render', function(event, form_id, current_page){
        starRating();
    });

    var size_li = $(".testimonial").size();
    console.log(size_li);
    var count=9;
    $('.testimonial:lt('+count+')').show();
    $('.load__more').click(function (e) {
        e.preventDefault();
        count= (count+9 <= size_li) ? count+9 : size_li;
        $('.testimonial:lt('+count+')').show();
        if(count == size_li){
            $('.load__more').hide();
        }
    });

    $(".testi__vid").wrapAll("<div class='testi__vid__wrap' />");

    $(".testi__vid__wrap").not(".slick-initialized").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        infinite: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

});

function starRating () {
    jQuery(".star__rating li input:checked").parent().addClass("active").prevAll().addClass("active");
    jQuery(".star__rating li").click(function () {
        jQuery(this).parent().find("li").removeClass("active");
        jQuery(this).addClass("active").prevAll().addClass("active");
    });

    jQuery('.multi__option select').multiSelect({
        noneText: 'Services*'
    });

    jQuery(".multi-select-menuitem").unbind("click").click(function (e) {
        e.preventDefault();
        console.log("test");
        jQuery(this).toggleClass("active");
        if(jQuery(this).find("input").prop("checked")==true) {
            jQuery(this).find("input").prop("checked", false);
        }
        else {
            jQuery(this).find("input").prop("checked", true);
        }
    });

    var selectName = jQuery(".multi__option select").attr("name");

    jQuery(".multi-select-menuitem").each(function () {
        jQuery(this).find("input").attr("name", selectName);
    })

}


(function($) {
})(jQuery);