jQuery(document).ready(function($) {

    "use strict";

    var getUrl = window.location;
    const _BASE_URL_ = getUrl.protocol + "//" + getUrl.host;
    const _FILES_URL_ = _BASE_URL_ + "/app/themes/BBTFramework/files/";
    const _DOWNLOAD_URL_ = _BASE_URL_ + "/will-kits/download/";


    function isiPhone(){
        return (
            (navigator.platform.indexOf("iPhone") != -1) ||
            (navigator.platform.indexOf("iPad") != -1) ||
            (navigator.platform.indexOf("iPod") != -1)
        );
    }



    $(window).load(function(e) {
        loadBannerHeight();
    });

    $(window).bind('orientationchange', function (event) {
    //    location.reload(true);
        loadBannerHeight();
    });

    $(window).resize(function(e) {
        loadBannerHeight();
    });


    $(window).on('load', function () {

        $('#header').wrapInner("<div class='header-inner haven-container'></div>");

        $('#footer').wrapInner("<div class='footer-inner'></div>");



        $(function() {
          $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html, body').animate({
                  scrollTop: target.offset().top
                }, 1000);
                return false;
              }
            }
          });
        });

    });


    $.fn.equalizeHeights = function(){
        return this.height( Math.max.apply(this, $(this).map(function (i,e){ return $(e).height() }).get()));
    }
    setInterval(function(){
        var success = $('#gform_confirmation_wrapper_8').length;
        if(success == 1) {
            $('#success-contact').fadeIn();
            $('#contact-send').fadeOut();
        }
    },500);

    function loadBannerHeight() {
        if( $(window).width() < 980 && $(window).width() > 480 ) {
            var vidHeight = $('.forIpad').height();
        } else if ( $(window).width() < 480 ) {
            var vidHeight = $('.forMobile').height();
        } else {
            var vidHeight = $('.vidImgBanner video').height();
        }
        $('.top-home .bg').css({'height': vidHeight });
    }
});

