<?php get_header(); ?>

    <!-- #content Starts -->
    <?php woo_content_before(); ?>
    <div id="content" class="col-full search-results">
    
        <div id="main-sidebar-container">    

            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main">
                <div class="blog-header">
                    <div class="blog-header--body">
                        <h1>Search Results</h1>
                        <p>Search the Haven website</p>
                    </div>
                </div>
                <div class="haven-container-a">
                    <div class="row">
                        <div class="post-search col-12"><?php get_search_form(); ?></div>
                    </div>
                </div>
                <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_sep_color_grey ks-blue-arrow">
                    <span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span>
                    <div class="vc_icon_element vc_icon_element-outer vc_icon_element-align-left">
                        <div class="vc_icon_element-inner vc_icon_element-color-blue vc_icon_element-size-md vc_icon_element-style- vc_icon_element-background-color-grey">
                            <img src="/app/uploads/2017/08/arrow-blue-down.png" />
                        </div>
                    </div>
                    <span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
                </div>
                <div class="haven-container-a">
                    <div class="row">
                        <div class="post-search-result col-12 results-title">
                            <?php
                                global $wp_query;

                                $count = $wp_query->found_posts;

                                echo 'We found ' . $count . ' results matching your search for <br />"' . esc_html($_GET['s'] . '"');

                                wp_reset_postdata();
                            ?>
                        </div>
                    </div>
                </div>
                <div class="haven-container-a post-grid search-grid1 All1">
                    <div class="row">
                    <?php
                    woo_loop_before();

                    global $query_string;

                    //Protect against arbitrary paged values
                    $current = get_query_var( 'paged' ) > 1 ? get_query_var('paged') : 1;          

                    wp_parse_str( $query_string, $search_query );

                    $the_query = new WP_Query( $search_query );

                    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
                    ?>
                        <div class="col-md-12 search-col">
                            <div class="post-content-wrapper">
                                <div class="results-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                <div class="results-post-title mobile">
                                    <a href="<?php the_permalink(); ?>"><?php echo limit_text(get_the_title(), 8) ?></a>
                                </div>
                                <?php if(get_post_type() == "post") : 
                                    $excerpt = get_excerpt();
                                else : //if page
                                    $excerpt = vc_excerpt_stripper(get_the_content());
                                endif; ?>

                                <div class="results-post-content"><p><?php echo limit_text($excerpt, 45) ?></p></div>
                                <div class="results-post-content ipad"><p><?php echo limit_text($excerpt, 23) ?></p></div>
                                <div class="results-post-content mobile"><p><?php echo limit_text($excerpt, 13) ?></p></div>
                            </div>
                        </div>
                    <?php endwhile; wp_reset_postdata(); ?>

                    <div class="col-md-12 pagination_wrapper">
                        <div class="pagination_links">
                            <?php
                            $big = 999999999; // need an unlikely integer
                             
                            echo paginate_links( array(
                                'base' => @add_query_arg('paged','%#%'),
                                'format' => '',
                                'current' => $current,
                                'prev_text' => '',
                                'next_text' => '',
                                'mid_size' => '4',
                                'end_size' => '1',
                                'total' => $the_query->max_num_pages
                            ) );
                            ?> 
                        </div>

                        <div class="pagination_links mobile">
                            <div class="arrow">
                                <?php if( $current > 1 && $current < $the_query->max_num_pages) : ?>
                                    <?php posts_nav_link(' ','<','>'); ?>
                                <?php endif; ?>

                                <?php if ($current == 1 && $current < $the_query->max_num_pages) : ?>
                                    <a class="disabled" href="#">&lt;</a> 
                                    <a href="/page/<?php echo $current + 1 ?>/?s=<?php echo esc_html($_GET['s']) ?>">&gt;</a>
                                <?php endif; ?>

                                <?php if( $current > 1 && $current == $the_query->max_num_pages) : ?> 
                                    <a href="/page/<?php echo $the_query->max_num_pages - 1 ?>/?s=<?php echo esc_html($_GET['s']) ?>">&gt;</a>
                                    <a class="disabled" href="#">&gt;</a>
                                <?php endif; ?>
                            </div>
                            <div class="numbers">
                                <?php if( $current > 1 && $current < $the_query->max_num_pages) : ?>
                                    <?php posts_nav_link('/', $current, $the_query->max_num_pages); ?>
                                <?php endif; ?>

                                <?php if ($current == 1 && $current < $the_query->max_num_pages) : ?>
                                    <a class="disabled" href="#">1</a>
                                    /
                                    <a href="/page/<?php echo $current + 1 ?>/?s=<?php echo esc_html($_GET['s']) ?>"><?php echo $the_query->max_num_pages ?></a>
                                <?php endif; ?>
                                
                                <?php if( $current > 1 && $current == $the_query->max_num_pages) : ?> 
                                    <a href="/page/<?php echo $the_query->max_num_pages - 1 ?>/?s=<?php echo esc_html($_GET['s']) ?>">
                                        <?php echo $the_query->max_num_pages ?>
                                    </a>
                                    /
                                    <a class="disabled" href="#"><?php echo $the_query->max_num_pages ?></a>
                                <?php endif; ?>
                            </div> 
                        </div>
                    </div>

                    <?php endif; wp_reset_query();
                    woo_loop_after();
                    ?> 
                    </div>
                </div>
            </section><!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>

        </div><!-- /#main-sidebar-container -->         

        <?php get_sidebar( 'alt' ); ?>

    </div><!-- /#content -->
    <?php woo_content_after(); ?>

<?php get_footer(); ?>