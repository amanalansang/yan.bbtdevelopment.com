<?php
//require_once get_stylesheet_directory_uri() . '/wp-bootstrap-navwalker.php';
require_once 'includes/kiwisaver-calc.php';
require_once 'includes/optimization.php';

register_nav_menus( array(
    'footer_menu' => 'Footer Menu',
) );


add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {

    if (!is_front_page()){
        wp_enqueue_style( 'material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
        wp_enqueue_style( 'reset-megamenu',get_stylesheet_directory_uri() . '/css/reset_megamenu.css' );
        wp_enqueue_style( 'style-megamenu', get_stylesheet_directory_uri() . '/css/style_megamenu.css' );
        wp_enqueue_style( 'style-calc', get_stylesheet_directory_uri() . '/css/calc.css' );
        wp_enqueue_style( 'bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css' );
        wp_enqueue_style( 'jquery-ui', get_stylesheet_directory_uri() . '/css/jquery-ui.min.css' );
        wp_enqueue_style( 'jquery-ui-theme', get_stylesheet_directory_uri() . '/css/jquery-ui.theme.min.css' );
        wp_enqueue_style( 'jquery-ui-structure', get_stylesheet_directory_uri() . '/css/jquery-ui.structure.min.css' );
        wp_enqueue_style( 'bxslider', get_stylesheet_directory_uri() . '/js/jquery.bxslider.css' );
        wp_enqueue_style( 'nice-select', get_stylesheet_directory_uri() . '/css/nice-select.css' );
        wp_enqueue_style( 'fancybox', get_stylesheet_directory_uri() . '/css/fancybox/jquery.fancybox.css' );
        wp_enqueue_style( 'animate', get_stylesheet_directory_uri() . '/css/animate.css' );
        wp_enqueue_style( 'willkits', get_stylesheet_directory_uri() . '/css/willkit.css' );
        wp_enqueue_style( 'story', get_stylesheet_directory_uri() . '/css/our-story.css' );
        wp_enqueue_style( 'prezzy', get_stylesheet_directory_uri() . '/prezzy.css' );
        wp_enqueue_style( 'custom', get_stylesheet_directory_uri() . '/css/custom.css' );
        wp_enqueue_style( 'footer', get_stylesheet_directory_uri() . '/css/footer.css' );
        wp_enqueue_style( 'popup', get_stylesheet_directory_uri() . '/css/popup.css' );

        if ( is_search() ) {
            wp_enqueue_style( 'search', get_stylesheet_directory_uri() . '/css/search.css' );
        }

        wp_enqueue_style( 'search-header', get_stylesheet_directory_uri() . '/css/search-header.css' );
        wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

        if(is_page_template('template-blog.php') || is_home() || is_single()) {
            wp_enqueue_style( 'blog-slick-css', get_stylesheet_directory_uri() . '/css/slick/slick.css'  );
            wp_enqueue_style( 'blog-slick-css-theme', get_stylesheet_directory_uri() . '/css/slick/slick-theme.css' );
            wp_enqueue_style( 'blog', get_stylesheet_directory_uri() . '/css/blog.css' );
        }
    }

     if (is_front_page()){
         wp_enqueue_style( 'bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css' );
         wp_enqueue_style( 'reset-megamenu',get_stylesheet_directory_uri() . '/css/reset_megamenu.css' );
         wp_enqueue_style( 'style-megamenu', get_stylesheet_directory_uri() . '/css/style_megamenu.css' );
         wp_enqueue_style( 'jquery-ui', get_stylesheet_directory_uri() . '/css/jquery-ui.min.css' );
         wp_enqueue_style( 'jquery-ui-theme', get_stylesheet_directory_uri() . '/css/jquery-ui.theme.min.css' );
         wp_enqueue_style( 'custom', get_stylesheet_directory_uri() . '/css/custom.css' );
         wp_enqueue_style( 'footer', get_stylesheet_directory_uri() . '/css/footer.css' );
         wp_enqueue_style( 'search-header', get_stylesheet_directory_uri() . '/css/search-header.css' );
         wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
     }
     wp_enqueue_style( 'slick-css', get_stylesheet_directory_uri() . '/css/vendor/slick.css' );
     wp_enqueue_style( 'slick-theme-css', get_stylesheet_directory_uri() . '/css/vendor/slick-theme.css' );
     wp_enqueue_style( 'style-updates-css', get_stylesheet_directory_uri() . '/css/style-updates.css' );
    wp_enqueue_style( 'custom-testimonials', get_stylesheet_directory_uri() . '/custom-testimonials.css' );
}

/** Adding custom script **/
add_action('wp_enqueue_scripts', 'bbt_custom_scripts', 5);

function bbt_custom_scripts() {

    //The only scripts needed on homepage
    if (is_front_page()){
        wp_enqueue_script( 'megamenu-js', get_stylesheet_directory_uri() . '/js/jquery.menu-aim.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'aim_megamenu-js', get_stylesheet_directory_uri() . '/js/main_megamenu.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'search-header', get_stylesheet_directory_uri() . '/js/search-header.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'homejs', get_stylesheet_directory_uri() . '/js/home.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'videos', get_stylesheet_directory_uri() . '/js/custom/videos.js', '', true  );
    }

    if (!is_front_page()){
        wp_enqueue_script( 'jquery-ui', get_stylesheet_directory_uri() . '/js/jquery-ui.js', array( 'jquery' ), '', true  );
        //wp_enqueue_script( 'bxslider', get_stylesheet_directory_uri() . '/js/jquery.bxslider.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'bxslider', get_stylesheet_directory_uri() . '/js/jquery.bxslider.min.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'responsive-mapping', get_stylesheet_directory_uri() . '/js/jquery.rwdImageMaps.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'match-height', get_stylesheet_directory_uri() . '/js/jquery.matchHeight.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'nice-select-js', get_stylesheet_directory_uri() . '/js/jquery.nice-select.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'bbt-custom-js', get_stylesheet_directory_uri() . '/js/scripts.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'calculator-custom-js', get_stylesheet_directory_uri() . '/js/calculator.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'megamenu-js', get_stylesheet_directory_uri() . '/js/jquery.menu-aim.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'aim_megamenu-js', get_stylesheet_directory_uri() . '/js/main_megamenu.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'modernizr-js', get_stylesheet_directory_uri() . '/js/modernizr.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'calc-extra-js', get_stylesheet_directory_uri() . '/js/calc-extra.js', array( 'jquery' ), '', true  );
        wp_enqueue_script( 'search-header', get_stylesheet_directory_uri() . '/js/search-header.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'fancybox', get_stylesheet_directory_uri() . '/js/fancybox/jquery.fancybox.js', array( 'jquery' ), '', true  );

        if(! is_page('are-you-on-the-right-plan') || ! is_page('haven-accounting') || ! is_page('accounting') || ! is_page_template('template-blog.php') || ! is_single() ) {
            wp_enqueue_script( 'jquery-validate', get_stylesheet_directory_uri() . '/js/jquery.validate.min.js', array( 'jquery' ), '', true  );
            wp_enqueue_script( 'owl-main', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), '', true  );
            wp_enqueue_script( 'owl-autoplay', get_stylesheet_directory_uri() . '/js/owl.autoplay.js', array( 'jquery' ), '', true  );
            wp_enqueue_script( 'owl-video', get_stylesheet_directory_uri() . '/js/owl.video.js', array( 'jquery' ), '', true );
            wp_enqueue_script( 'story', get_stylesheet_directory_uri() . '/js/story.js', '', true  );
        }

        if(is_page_template('template-blog.php') || is_home() || is_single()) {
            wp_enqueue_script( 'blog-js', get_stylesheet_directory_uri() . '/js/blog.js', array( 'jquery' ), '', true  );
        }
        wp_enqueue_script( 'prezzy', get_stylesheet_directory_uri() . '/js/main-prezzy.js', '', true  );
        //wp_enqueue_script( 'owl', get_stylesheet_directory_uri() . '/js/owl.autoplay.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'main', get_stylesheet_directory_uri() . '/js/main.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'videos', get_stylesheet_directory_uri() . '/js/custom/videos.js', '', true  );
    }

    wp_enqueue_script( 'slick-js', get_stylesheet_directory_uri() . '/js/vendor/slick.min.js', array( 'jquery' ), '', true  );
    wp_enqueue_script( 'style-updates-js', get_stylesheet_directory_uri() . '/js/style-updates.js', array( 'jquery' ), '', true  );
    wp_enqueue_script( 'multiselect-js', get_stylesheet_directory_uri() . '/js/jquery.multi-select.min.js', array( 'jquery' ), '', true  );
    wp_enqueue_script( 'blog-slick-js', get_stylesheet_directory_uri() . '/js/slick/slick.js', array( 'jquery' ), '', true  );
    wp_enqueue_script( 'testimonials-js', get_stylesheet_directory_uri() . '/js/testimonials.js', array( 'jquery' ), '', true  );
}

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
    } else {
        $excerpt = implode(" ",$excerpt);
    }
    $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
    return $excerpt;
}

/** MOST READ POSTS */
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
//To keep the count accurate, lets get rid of prefetching
// remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

// function wpb_get_post_views($postID){
//     $count_key = 'wpb_post_views_count';
//     $count = get_post_meta($postID, $count_key, true);
//     if($count==''){
//         delete_post_meta($postID, $count_key);
//         add_post_meta($postID, $count_key, '0');
//         return "0 View";
//     }
//     return $count.' Views';
// }

// function wpb_track_post_views ($post_id) {
//     if ( !is_single() ) return;
//     if ( empty ( $post_id) ) {
//         global $post;
//         $post_id = $post->ID;
//     }
//     wpb_set_post_views($post_id);
// }
// add_action( 'wp_head', 'wpb_track_post_views');
/** END MOST READ POSTS */

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');
/** END MOST READ POSTS */

// Moves Main Navigation to the top of the header
add_action( 'init', 'woo_custom_move_navigation', 10 );

function woo_custom_move_navigation () {
    // Remove main nav from the woo_header_after hook
    remove_action( 'woo_header_after','woo_nav', 10 );
    // Add main nav to the woo_header_inside hook
    add_action( 'woo_header_inside','woo_nav', 10 );
}   // End woo_custom_move_navigation()


add_action('woo_footer_before', 'footer_newsletter', 10);

function footer_newsletter(){
    echo "<div class='footer-newsletter-inner'>";
    /*gravity_form(1, false, true, false, '', true, 12);*/
    echo "
        <!-- Begin MailChimp Signup Form -->
        <div class='footer-newsletter_wrapper'>
            <div id='mc_embed_signup' class='footer-newsletter'>
                <form action='https://havenadvisers.us1.list-manage.com/subscribe/post?u=6f29bedba4256214f525db301&amp;id=9e521f239b' method='post' id='mc-embedded-subscribe-form' name='mc-embedded-subscribe-form' class='validate' target='_blank' novalidate>
                    <div id='mc_embed_signup_scroll' class='mc-new-form'>
                        <div class='gform_heading'>
                            <span class='gform_description'>Sign up to our newsletter</span>
                        </div>
                        <div class='mc-field-group top_label'>
                            <input type='text' value='' name='FNAME' class='required' id='mce-FNAME' placeholder='First Name'  required/>
                        </div>
                        <div class='mc-field-group top_label'>
                            <input type='text' value='' name='MMERGE2' class='required' id='mce-MMERGE2' placeholder='Last Name'  required/>
                        </div>
                        <div class='mc-field-group top_label'>
                            <input type='email' value='' name='EMAIL' class='required email' id='mce-EMAIL' placeholder='Email Address' required/>
                        </div>
                        <div class='mc-field-group top_label  gform_footer'><input type='submit' value='Subscribe' name='subscribe' id='mc-embedded-subscribe' class='button'></div>
                        <div id='mce-responses' class='clear'>
                            <div class='response' id='mce-error-response' style='display:none'></div>
                            <div class='response' id='mce-success-response' style='display:none'></div>
                        </div>
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style='position: absolute; left: -5000px;' aria-hidden='true'><input type='text' name='b_6f29bedba4256214f525db301_9e521f239b' tabindex='-1' value=''></div>
                    </div>
                </form>
            </div>
        </div>
        <!--End mc_embed_signup-->
    ";
    echo "</div>";
}

add_action('woo_footer_inside', 'footer_logo', 10);

function footer_logo(){
    echo "<div class='footer-logo'><img src='/app/themes/BBTFramework/images/logo-white.png'></div>";
    wp_nav_menu( array( 'theme_location' => 'footer_menu' ) );
}

add_filter( 'body_class', 'add_slug_body_class' );

function add_slug_body_class( $classes ) {
    if( is_page() ) { 
            $parents = get_post_ancestors( get_the_ID() );
            $id = ($parents) ? $parents[count($parents)-1]: get_the_ID();
        if ($id) {
            $classes[] = 'top-parent-' . $id;
        } else {
            $classes[] = 'top-parent-' . get_the_ID();
        }
    }
 
    return $classes;
}

add_filter( 'body_class', 'add_slug_body_class_2' );

function add_slug_body_class_2( $classes ) {
    global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}

function get_excerpt(){
    $excerpt = get_the_content();
    $excerpt = preg_replace(" ([.*?])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = trim($excerpt, " ");
    $excerpt = str_replace("&nbsp;", '', $excerpt);
    $excerpt = substr($excerpt, 0, 300);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    if($excerpt) {
        $excerpt = $excerpt.'...';
    }
    return $excerpt;
}

function html5_search_form( $form ) {
    global $post;    
    $form = "
        <div class='search_main'>
            <form role='search' method='get' class='searchform' action='" . home_url( '/' ) . "' >
                <input type='search' class='field s' value='' name='s' id='s' placeholder='Search our site' />
                <button type='submit' class='fa fa-search submit' name='submit' value='Search'></button>
            </form>
            <div class='fix'></div>
        </div>
    ";
    return $form;
}
 
add_filter( 'get_search_form', 'html5_search_form' );

function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

function vc_excerpt_stripper($the_content) {
    $shortcode_tags = array('VC_ROW');
    $values = array_values( $shortcode_tags );
    $exclude_codes  = implode( '|', $values );

    // strip all shortcodes but keep content
    $the_content = preg_replace("~(?:\[/?)[^/\]]+/?\]~s", '', $the_content);
    // strip remaining shortcode tags
    $the_content = strip_shortcodes($the_content);
    // remove other tags other than shortcodes
    $the_content = strip_tags($the_content);
    // return remaining texts
    return $the_content;
}

if (!is_admin()) {
    function wpb_search_filter($query) {
        if ($query->is_search()) {
            $query->set('post_type', array('post', 'page', 'blog'));
        }
        return $query;
    }
    add_filter('pre_get_posts','wpb_search_filter');
}

add_filter('posts_orderby','my_sort_custom',10,2);
function my_sort_custom( $orderby, $query ){
    global $wpdb;

    if(!is_admin() && is_search()) 
        $orderby =  $wpdb->prefix."posts.post_type ASC";

    return  $orderby;
}

add_shortcode('haven_request_meeting', 'request_meeting');

function request_meeting(){
    $text = "
        <div class='free-meeting request-free-meeting'>
            <div class='free-meeting-inner'><p>Talk to us and see if this fund suits your needs</p></div>
            <div class='button-white free-meeting-inner'><a href='/contact-us'>Request a free meeting</a></div>
        </div>
    ";
    return $text;
}

add_shortcode('haven_custom_cto', 'custom_cto');

function custom_cto($post){
    $caption = $post['caption'];
    $hyperlink = $post['hyperlink'];
    $cto_text = $post['cto_text'];
    $text = "
        <div class='free-meeting request-free-meeting'>
            <div class='free-meeting-inner'><p>".$caption."</p></div>
            <div class='button-white free-meeting-inner'><a href='".$hyperlink."'>".$cto_text."</a></div>
        </div>
    ";
    return $text;
}

add_shortcode('haven_tools_guides', 'tools_guides');

function tools_guides(){
    $text = "
        <div class='tools-guides haven-container'>
            <h2>Still unsure?</h2>
            <p>Read our Tools &amp; Guides for more information</p>
                <div class='vc_row ks-unsure'>
                    <div class='vc_col-sm-4'>
                    <h4>KiwiSaver</h4>
                    <hr/>
                    <ul>
                    <li><a href='#'>KiwiSaver HomeStart grant explained</a></li>
                    <li><a href='#'>What’s happening in the property market?</a></li>
                    <li><a href='#'>Trust law reform</a></li>
                    <li><a href='#'>Tax update – Simplification of taxes, foreign …</a></li>
                    <li><a href='#'>What do the new lending rules mean?</a></li>
                    </ul>
                        </div>
                        <div class='vc_col-sm-4'>
                        <h4>First home buyers</h4>
                        <hr/>
                    <ul>
                    <li><a href='#'>KiwiSaver HomeStart grant explained</a></li>
                    <li><a href='#'>What’s happening in the property market?</a></li>
                    <li><a href='#'>Trust law reform</a></li>
                    <li><a href='#'>Tax update – Simplification of taxes, foreign …</a></li>
                    <li><a href='#'>What do the new lending rules mean?</a></li>
                    </ul>
                        </div>
                        <div class='vc_col-sm-4'>
                        <h4>Mortgages</h4>
                        <hr/>
                    <ul>
                        <li><a href='#'>KiwiSaver HomeStart grant explained</a></li>
                        <li><a href='#'>What’s happening in the property market?</a></li>
                        <li><a href='#'>Trust law reform</a></li>
                        <li><a href='#'>Tax update – Simplification of taxes, foreign …</a></li>
                        <li><a href='#'>What do the new lending rules mean?</a></li>
                    </ul>
                </div>
            </div>
        </div>
    ";
    //return $text;
}

add_shortcode ('haven_other_services', 'other_services');

function other_services(){
    $text = "
        <div class='other-services'>
            <div class='haven-container'>
                <h2>We think you might also like</h2>
                    </div>
                    <div class='vc_row haven-container'>
                        <div class='vc_col-sm-3'>
                        <img src='/app/uploads/2017/08/trusts.png'>
                        <h4>Trusts</h4>
                    <div class='benefits-text'><p>Keep your assets with their rightful owner. A Haven adviser can help you set up a trust and even act as a trustee.</p></div>
                    <div class='button-white-black'>
                        <a href='#'>Learn more</a>
                        </div>
                        </div>
                        <div class='vc_col-sm-3'>
                        <img src='/app/uploads/2017/08/mortgae-advice.png'>
                        <h4>Mortgage Advice</h4>
                    <div class='benefits-text'><p>Whether you’re looking to buy your first home, invest in property, or pay off your mortgage faster, a Haven mortgage adviser can help.</p></div>
                    <div class='button-white-black'>
                        <a href='#'>Learn more</a>
                        </div>
                        </div>
                        <div class='vc_col-sm-3'>
                        <img src='/app/uploads/2017/08/home-insurance.png'>
                        <h4>Home Insurance</h4>
                    <div class='benefits-text'><p>We understand the importance of a loved home and treasured possessions. A Haven insurance adviser can help you keep the roof over your head.</p></div>
                    <div class='button-white-black'>
                        <a href='#'>Learn more</a>
                        </div>
                        </div>
                        <div class='vc_col-sm-3'>
                        <img src='/app/uploads/2017/08/car-insurance.png'>
                        <h4>Car Insurance</h4>
                    <div class='benefits-text'><p>A Haven adviser can help protect your prized wheels against loss or damage from an accident, fire or theft.</p></div>
                    <div class='button-white-black'>
                        <a href='#'>Learn more</a>
                    </div>
                </div>
            </div>
        </div>
    ";
    return $text;

}

add_shortcode('haven_right_fund_type', 'right_fund_type');

function right_fund_type(){
    $text = "
        <div class='right-fund-type haven-container-inner'>
            <div>
            <h2>How much could you save?</h2>
                <form>
                    <div class='save-section'>I was born in <select><option>January</option></select> <select><option>2003</option></select></div>
                    <div class='save-section'>My annual income is <input type='text' value='$2,000,000' /> and my current KiwiSaver balance is <input type='text' value='$1,234,567.89' /></div>
                    <div class='save-section'>I will contribute <select><option>8%</option></select> of my income  and my employer will contribute <select><option>8%</option></select> on top.</div>
                </form>
                <div class='read-assumptions'>
                    <p>Assumptions</p><hr/>
                    <div class='assumptions'>
                        <p>These graphs are examples only to help you understand how different investment choices may affect KiwiSaver savings and retirement income. The figures and data used are for illustration only and may not reflect actual returns and actual balances. The starting salary is assumed to be $35,000. Please note that returns are likely to fluctuate due to investment and other risks, including loss of capital invested. Returns may be negative in some periods. These graphs are not intended to convey personalised advice, and we recommend that you consult with your financial adviser or a BNZ Authorised Financial Adviser before making any decisions.</p>
                        <p>The figures in these graphs use assumptions consistent with those used on the Sorted website (based on January 2015 assumptions) and are as follows: (1) they have been adjusted for the effect of inflation at the rate of 2% so that the results are shown in today’s dollars, (2) employer contributions of 3% of the stated before-tax salary are taken into account after deduction of employer’s superannuation contribution tax at current applicable rates, (3) Government annual contributions appropriate to the contributions made and at today’s levels only are taken into account, (4) salaries will increase by 3.5% each year (1.5% increase plus 2% for inflation), (5) you take no contribution holidays (6) no amounts are withdrawn, (7) positive investment performance each year of 3.6% per annum for an investor in the Conservative Fund and 5.4% per annum for an investor in the Growth Fund net of fees and taxes using a Prescribed Investor (tax) Rate (PIR) of 17.5%, and (8) post retirement monthly income amounts for all examples are based on: (a) regular withdrawals of the amounts shown being made at the end of each month for a period of 25 years from age 65 to age 90, (b) positive investment performance each year of 3.6% per annum for an investor in the Conservative Fund and 5.4% per annum for an investor in the Growth Fund net of fees and taxes using a Prescribed Investor (tax) Rate (PIR) of 17.5%, and (c) no capital remaining at the end of the period.</p>
                        <p>To be eligible, you must be 18 years or older and and mainly live in New Zealand. If you don't meet these requirements for the full MTC year, the maximum amount you'll get will be based on the time you are eligible for.</p>
                    </div>
                </div>
            </div>
        </div>
    ";
    return $text;

}

add_shortcode('haven_value_contents', 'value_contents');

function value_contents() {
    $text = '
        <div id="mortgageList" class="value-contents">
              <div class="m-l-tabs-header col-lg-3">
                  <ul>
                    <li><a href="#living"><i class="material-icons">weekend</i>Living areas</a></li>
                    <li><a href="#kitchen"><i class="material-icons">restaurant</i>Kitchen</a></li>
                    <li><a href="#bathroom"><i class="material-icons">hot_tub</i>Bathroom</a></li>
                    <li><a href="#laundry"><i class="material-icons">local_laundry_service</i>Laundry</a></li>
                    <li><a href="#office"><i class="material-icons">computer</i>Office</a></li>
                    <li><a href="#bedroom1"><i class="material-icons">local_hotel</i>Master bedroom</a></li>
                    <li><a href="#bedroom2"><i class="material-icons">local_hotel</i>Bedroom 2</a></li>
                    <li><a href="#bedroom3"><i class="material-icons">local_hotel</i>Bedroom 3</a></li>
                    <li><a href="#general"><i class="material-icons">camera_alt</i>General/personal effects</a></li>
                    <li><a href="#garage"><i class="material-icons">directions_car</i>Garage/workshop</a></li>
                    <li><a href="#contenttotal">Contents Total</a></li>
                  </ul>
              </div>
              <div class="m-l-tabs col-lg-9">
                  <div id="living">
                    <div class="l-t">
                        <div class="l-t-h">
                            <div class="row">
                                <div class="col-12">Living areas</div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-lg-3"><span class="d-t">Living areas</span><span class="m-t">Item</span></div>
                                <div class="col-3">Average</div>
                                <div class="col-3">Above Average</div>
                                <div class="col-6 col-lg-3">Your value</div>
                            </div>
                        </div>
                        <div class="l-t-b">
                            <div class="l-t-b-i">
                                <div class="row">
                                    <div class="col-6 col-lg-3">Lounge Suites / Couches</div>
                                    <div class="col-3 average"><span class="calc_val">2500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">6500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Books</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">5500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bookshelves/ Shelves</div>
                                    <div class="col-3 average"><span class="calc_val">1500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Artwork / Ornaments</div>
                                    <div class="col-3 average"><span class="calc_val">800</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">4000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Cabinets / Wall Units</div>
                                    <div class="col-3 average"><span class="calc_val">1200</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2140</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">China Cabinets</div>
                                    <div class="col-3 average"><span class="calc_val">1200</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3210</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">CDs / DVDs / Videos / Records</div>
                                    <div class="col-3 average"><span class="calc_val">1200</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">5350</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Stereo / Speakers / Sound System</div>
                                    <div class="col-3 average"><span class="calc_val">1100</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3210</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">DVD Player/ Video Player / Recorder</div>
                                    <div class="col-3 average"><span class="calc_val">500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Television</div>
                                    <div class="col-3 average"><span class="calc_val">1500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">5000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Crystal and Silverware</div>
                                    <div class="col-3 average"><span class="calc_val">750</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2140</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Cushions / Rugs</div>
                                    <div class="col-3 average"><span class="calc_val">320</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">535</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Clocks</div>
                                    <div class="col-3 average"><span class="calc_val">105</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">535</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Lamps</div>
                                    <div class="col-3 average"><span class="calc_val">320</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">535</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Unfixed Mirrors</div>
                                    <div class="col-3 average"><span class="calc_val">160</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">535</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Dining Table &amp; Chairs</div>
                                    <div class="col-3 average"><span class="calc_val">1500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">5000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Alcohol</div>
                                    <div class="col-3 average"><span class="calc_val">320</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">855</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Coffee table</div>
                                    <div class="col-3 average"><span class="calc_val">1300</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Miscellaneous</div>
                                    <div class="col-3 average"><span class="calc_val">0</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">0</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                            </div>
                        </div>
                        <div class="l-t-f">
                            <div class="row">
                                <div class="col-6 col-lg-3">Total Living areas</div>
                                <div class="col-3 t-average living-a">$33695</div>
                                <div class="col-3 t-a-average living-a-a">$100745</div>
                                <div class="col-6 col-lg-3 t-y-v living-v">$0</div>
                            </div>
                        </div>
                    </div>
                    <div class="m-l-tabs-pn"></div>
                  </div>
                  <div id="kitchen">
                    <div class="l-t">
                        <div class="l-t-h">
                            <div class="row">
                                <div class="col-12">Kitchen</div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-lg-3"><span class="d-t">Kitchen</span><span class="m-t">Item</span></div>
                                <div class="col-3">Average</div>
                                <div class="col-3">Above Average</div>
                                <div class="col-6 col-lg-3">Your value</div>
                            </div>
                        </div>
                        <div class="l-t-b">
                            <div class="l-t-b-i">
                                <div class="row">
                                    <div class="col-6 col-lg-3">Chinaware</div>
                                    <div class="col-3 average"><span class="calc_val">960</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2140</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Coffee Maker</div>
                                    <div class="col-3 average"><span class="calc_val">100</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">800</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Crockery</div>
                                    <div class="col-3 average"><span class="calc_val">640</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1100</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Cutlery / Utensils</div>
                                    <div class="col-3 average"><span class="calc_val">550</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1100</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Dishwasher</div>
                                    <div class="col-3 average"><span class="calc_val">1200</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Jug / Kettle</div>
                                    <div class="col-3 average"><span class="calc_val">50</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">150</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Microwave</div>
                                    <div class="col-3 average"><span class="calc_val">300</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Mini Oven / Griller</div>
                                    <div class="col-3 average"><span class="calc_val">200</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">400</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Mixer / Blender</div>
                                    <div class="col-3 average"><span class="calc_val">150</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">400</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Toaster / Sandwich Maker</div>
                                    <div class="col-3 average"><span class="calc_val">150</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">300</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Saucepans / Pots / Bowls</div>
                                    <div class="col-3 average"><span class="calc_val">480</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1100</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Glass and Plasticware</div>
                                    <div class="col-3 average"><span class="calc_val">250</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Freezer</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Refrigerator</div>
                                    <div class="col-3 average"><span class="calc_val">1200</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Packaged / Preserved and Fresh Food</div>
                                    <div class="col-3 average"><span class="calc_val">550</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Linen / Place Settings</div>
                                    <div class="col-3 average"><span class="calc_val">215</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">460</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Pictures / Ornaments</div>
                                    <div class="col-3 average"><span class="calc_val">320</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">550</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Miscellaneous</div>
                                    <div class="col-3 average"><span class="calc_val">0</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">0</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                            </div>
                        </div>
                        <div class="l-t-f">
                            <div class="row">
                                <div class="col-6 col-lg-3">Total Kitchen</div>
                                <div class="col-3 t-average kitchen-a">$33695</div>
                                <div class="col-3 t-a-average kitchen-a-a">$100745</div>
                                <div class="col-6 col-lg-3 t-y-v kitchen-v">$0</div>
                            </div>
                        </div>
                    </div>
                    <div class="m-l-tabs-pn"></div>
                  </div>
                  <div id="bathroom">
                    <div class="l-t">
                        <div class="l-t-h">
                            <div class="row">
                                <div class="col-12">Bathroom</div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-lg-3"><span class="d-t">Bathroom</span><span class="m-t">Item</span></div>
                                <div class="col-3">Average</div>
                                <div class="col-3">Above Average</div>
                                <div class="col-6 col-lg-3">Your value</div>
                            </div>
                        </div>
                        <div class="l-t-b">
                            <div class="l-t-b-i">
                                <div class="row">
                                    <div class="col-6 col-lg-3">Hairdryer / Hair Stylers</div>
                                    <div class="col-3 average"><span class="calc_val">105</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">450</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Medicines</div>
                                    <div class="col-3 average"><span class="calc_val">125</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">550</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Scales</div>
                                    <div class="col-3 average"><span class="calc_val">70</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">200</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Cosmetics / Lotions / Perfumes etc</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Towels</div>
                                    <div class="col-3 average"><span class="calc_val">535</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Toiletries</div>
                                    <div class="col-3 average"><span class="calc_val">600</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Electric Shaver</div>
                                    <div class="col-3 average"><span class="calc_val">100</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">250</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Miscellaneous</div>
                                    <div class="col-3 average"><span class="calc_val">0</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">0</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                            </div>
                        </div>
                        <div class="l-t-f">
                            <div class="row">
                                <div class="col-6 col-lg-3">Total Bathroom</div>
                                <div class="col-3 t-average bathroom-a">$33695</div>
                                <div class="col-3 t-a-average bathroom-a-a">$100745</div>
                                <div class="col-6 col-lg-3 t-y-v bathroom-v">$0</div>
                            </div>
                        </div>
                    </div>
                    <div class="m-l-tabs-pn"></div>
                  </div>
                  <div id="laundry">
                    <div class="l-t">
                        <div class="l-t-h">
                            <div class="row">
                                <div class="col-12">Laundry</div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-lg-3"><span class="d-t">Laundry</span><span class="m-t">Item</span></div>
                                <div class="col-3">Average</div>
                                <div class="col-3">Above Average</div>
                                <div class="col-6 col-lg-3">Your value</div>
                            </div>
                        </div>
                        <div class="l-t-b">
                            <div class="l-t-b-i">
                                <div class="row">
                                    <div class="col-6 col-lg-3">Cleaning Equipment / Supplies</div>
                                    <div class="col-3 average"><span class="calc_val">100</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">300</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Clothes Basket</div>
                                    <div class="col-3 average"><span class="calc_val">55</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">160</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Dryer</div>
                                    <div class="col-3 average"><span class="calc_val">600</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1200</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Iron</div>
                                    <div class="col-3 average"><span class="calc_val">70</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">150</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Ironing Board</div>
                                    <div class="col-3 average"><span class="calc_val">75</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">100</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Unfixed Towel Rails</div>
                                    <div class="col-3 average"><span class="calc_val">150</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">320</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Vacuum Cleaner</div>
                                    <div class="col-3 average"><span class="calc_val">450</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Washing Machine</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Miscellaneous</div>
                                    <div class="col-3 average"><span class="calc_val">0</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">0</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                            </div>
                        </div>
                        <div class="l-t-f">
                            <div class="row">
                                <div class="col-6 col-lg-3">Total Laundry</div>
                                <div class="col-3 t-average laundry-a">$33695</div>
                                <div class="col-3 t-a-average laundry-a-a">$100745</div>
                                <div class="col-6 col-lg-3 t-y-v laundry-v">$0</div>
                            </div>
                        </div>
                    </div>
                    <div class="m-l-tabs-pn"></div>
                  </div>
                  <div id="office">
                    <div class="l-t">
                        <div class="l-t-h">
                            <div class="row">
                                <div class="col-12">Office</div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-lg-3"><span class="d-t">Office</span><span class="m-t">Item</span></div>
                                <div class="col-3">Average</div>
                                <div class="col-3">Above Average</div>
                                <div class="col-6 col-lg-3">Your value</div>
                            </div>
                        </div>
                        <div class="l-t-b">
                            <div class="l-t-b-i">
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bookcase</div>
                                    <div class="col-3 average"><span class="calc_val">350</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">650</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Computer / Laptop</div>
                                    <div class="col-3 average"><span class="calc_val">3500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">7000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Printers, Faxes, Scanners</div>
                                    <div class="col-3 average"><span class="calc_val">100</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">300</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Desk and Chairs</div>
                                    <div class="col-3 average"><span class="calc_val">535</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1070</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Filing Cabinet</div>
                                    <div class="col-3 average"><span class="calc_val">265</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">640</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Stationery</div>
                                    <div class="col-3 average"><span class="calc_val">215</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">535</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Miscellaneous</div>
                                    <div class="col-3 average"><span class="calc_val">0</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">0</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                            </div>
                        </div>
                        <div class="l-t-f">
                            <div class="row">
                                <div class="col-6 col-lg-3">Total Office</div>
                                <div class="col-3 t-average office-a">$33695</div>
                                <div class="col-3 t-a-average office-a-a">$100745</div>
                                <div class="col-6 col-lg-3 t-y-v office-v">$0</div>
                            </div>
                        </div>
                    </div>
                    <div class="m-l-tabs-pn"></div>
                  </div>
                  <div id="bedroom1">
                    <div class="l-t">
                        <div class="l-t-h">
                            <div class="row">
                                <div class="col-12">Master Bedroom</div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-lg-3"><span class="d-t">Master Bedroom</span><span class="m-t">Item</span></div>
                                <div class="col-3">Average</div>
                                <div class="col-3">Above Average</div>
                                <div class="col-6 col-lg-3">Your value</div>
                            </div>
                        </div>
                        <div class="l-t-b">
                            <div class="l-t-b-i">
                                <div class="row">
                                    <div class="col-6 col-lg-3">Alarm Clock / Radio</div>
                                    <div class="col-3 average"><span class="calc_val">75</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">200</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bedding / Linen</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bedroom Furniture</div>
                                    <div class="col-3 average"><span class="calc_val">900</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">4000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Chair</div>
                                    <div class="col-3 average"><span class="calc_val">105</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">535</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Electric Blanket</div>
                                    <div class="col-3 average"><span class="calc_val">100</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">250</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Lamps</div>
                                    <div class="col-3 average"><span class="calc_val">215</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">430</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bed / Mattress / Base</div>
                                    <div class="col-3 average"><span class="calc_val">1500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">5000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Ornaments / Artwork</div>
                                    <div class="col-3 average"><span class="calc_val">350</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">550</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">TV / DVD Player</div>
                                    <div class="col-3 average"><span class="calc_val">500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Miscellaneous</div>
                                    <div class="col-3 average"><span class="calc_val">0</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">0</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                            </div>
                        </div>
                        <div class="l-t-f">
                            <div class="row">
                                <div class="col-6 col-lg-3">Total Master Bedroom</div>
                                <div class="col-3 t-average bedroom1-a">$33695</div>
                                <div class="col-3 t-a-average bedroom1-a-a">$100745</div>
                                <div class="col-6 col-lg-3 t-y-v bedroom1-v">$0</div>
                            </div>
                        </div>
                    </div>
                    <div class="m-l-tabs-pn"></div>
                  </div>
                  <div id="bedroom2">
                    <div class="l-t">
                        <div class="l-t-h">
                            <div class="row">
                                <div class="col-12">Bedroom 2</div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-lg-3"><span class="d-t">Bedroom 2</span><span class="m-t">Item</span></div>
                                <div class="col-3">Average</div>
                                <div class="col-3">Above Average</div>
                                <div class="col-6 col-lg-3">Your value</div>
                            </div>
                        </div>
                        <div class="l-t-b">
                            <div class="l-t-b-i">
                                <div class="row">
                                    <div class="col-6 col-lg-3">Alarm Clock / Radio</div>
                                    <div class="col-3 average"><span class="calc_val">75</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">200</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bedding / Linen</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1200</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bedroom Furniture</div>
                                    <div class="col-3 average"><span class="calc_val">900</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Chair</div>
                                    <div class="col-3 average"><span class="calc_val">105</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">535</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Electric Blanket</div>
                                    <div class="col-3 average"><span class="calc_val">100</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">250</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Lamps</div>
                                    <div class="col-3 average"><span class="calc_val">215</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">430</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bed / Mattress / Base</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Ornaments / Artwork</div>
                                    <div class="col-3 average"><span class="calc_val">350</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">550</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">TV / DVD Player</div>
                                    <div class="col-3 average"><span class="calc_val">500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Miscellaneous</div>
                                    <div class="col-3 average"><span class="calc_val">0</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">0</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                            </div>
                        </div>
                        <div class="l-t-f">
                            <div class="row">
                                <div class="col-6 col-lg-3">Total Bedroom 2</div>
                                <div class="col-3 t-average bedroom2-a">$33695</div>
                                <div class="col-3 t-a-average bedroom2-a-a">$100745</div>
                                <div class="col-6 col-lg-3 t-y-v bedroom2-v">$0</div>
                            </div>
                        </div>
                    </div>
                    <div class="m-l-tabs-pn"></div>
                  </div>
                  <div id="bedroom3">
                    <div class="l-t">
                        <div class="l-t-h">
                            <div class="row">
                                <div class="col-12">Bedroom 3</div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-lg-3"><span class="d-t">Bedroom 3</span><span class="m-t">Item</span></div>
                                <div class="col-3">Average</div>
                                <div class="col-3">Above Average</div>
                                <div class="col-6 col-lg-3">Your value</div>
                            </div>
                        </div>
                        <div class="l-t-b">
                            <div class="l-t-b-i">
                                <div class="row">
                                    <div class="col-6 col-lg-3">Alarm Clock / Radio</div>
                                    <div class="col-3 average"><span class="calc_val">75</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">200</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bedding / Linen</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1200</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bedroom Furniture</div>
                                    <div class="col-3 average"><span class="calc_val">900</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Chair</div>
                                    <div class="col-3 average"><span class="calc_val">105</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">535</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Electric Blanket</div>
                                    <div class="col-3 average"><span class="calc_val">100</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">250</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Lamps</div>
                                    <div class="col-3 average"><span class="calc_val">215</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">430</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bed / Mattress / Base</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Ornaments / Artwork</div>
                                    <div class="col-3 average"><span class="calc_val">350</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">550</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">TV / DVD Player</div>
                                    <div class="col-3 average"><span class="calc_val">500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Miscellaneous</div>
                                    <div class="col-3 average"><span class="calc_val">0</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">0</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                            </div>
                        </div>
                        <div class="l-t-f">
                            <div class="row">
                                <div class="col-6 col-lg-3">Total Bedroom 3</div>
                                <div class="col-3 t-average bedroom3-a">$33695</div>
                                <div class="col-3 t-a-average bedroom3-a-a">$100745</div>
                                <div class="col-6 col-lg-3 t-y-v bedroom3-v">$0</div>
                            </div>
                        </div>
                    </div>
                    <div class="m-l-tabs-pn"></div>
                  </div>
                  <div id="general">
                    <div class="l-t">
                        <div class="l-t-h">
                            <div class="row">
                                <div class="col-12">General/personal effects</div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-lg-3"><span class="d-t">General/personal effects</span><span class="m-t">Item</span></div>
                                <div class="col-3">Average</div>
                                <div class="col-3">Above Average</div>
                                <div class="col-6 col-lg-3">Your value</div>
                            </div>
                        </div>
                        <div class="l-t-b">
                            <div class="l-t-b-i">
                                <div class="row">
                                    <div class="col-6 col-lg-3">Antiques</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Barbeque</div>
                                    <div class="col-3 average"><span class="calc_val">500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">5000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bicycles</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Binoculars</div>
                                    <div class="col-3 average"><span class="calc_val">150</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1200</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Cameras</div>
                                    <div class="col-3 average"><span class="calc_val">535</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Mobile Phones</div>
                                    <div class="col-3 average"><span class="calc_val">550</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1285</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Telephone</div>
                                    <div class="col-3 average"><span class="calc_val">200</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">300</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Collections (Stamps / Coins)</div>
                                    <div class="col-3 average"><span class="calc_val">1070</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1605</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Clocks</div>
                                    <div class="col-3 average"><span class="calc_val">160</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">320</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Clothing and Footwear</div>
                                    <div class="col-3 average"><span class="calc_val">8000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">16000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Curtains / Blinds</div>
                                    <div class="col-3 average"><span class="calc_val">3000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">10000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Dehumidifier</div>
                                    <div class="col-3 average"><span class="calc_val">400</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Dentures</div>
                                    <div class="col-3 average"><span class="calc_val">1080</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Game console/accessories/games</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">4000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Games / Toys etc</div>
                                    <div class="col-3 average"><span class="calc_val">640</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1070</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Outdoor Portable Gas Heater</div>
                                    <div class="col-3 average"><span class="calc_val">600</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Hearing Aids</div>
                                    <div class="col-3 average"><span class="calc_val">3500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">10000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Hobby Equipment</div>
                                    <div class="col-3 average"><span class="calc_val">535</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2140</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Jewellery / Watches</div>
                                    <div class="col-3 average"><span class="calc_val">3000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">5000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Unfixed Mirrors</div>
                                    <div class="col-3 average"><span class="calc_val">270</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">430</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Musical Instruments</div>
                                    <div class="col-3 average"><span class="calc_val">215</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1070</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Artwork / Ornaments</div>
                                    <div class="col-3 average"><span class="calc_val">320</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2675</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Portable music player (iPod)</div>
                                    <div class="col-3 average"><span class="calc_val">200</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">400</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Plant Containers &amp; Pots</div>
                                    <div class="col-3 average"><span class="calc_val">500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Sewing Machine</div>
                                    <div class="col-3 average"><span class="calc_val">550</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Spectacles / Glasses (each)</div>
                                    <div class="col-3 average"><span class="calc_val">650</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1200</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Sports Equipment</div>
                                    <div class="col-3 average"><span class="calc_val">500</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2200</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Suitcases / Bags / Purses</div>
                                    <div class="col-3 average"><span class="calc_val">600</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">1000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Unfixed Heaters / Fans / Dehumidifiers</div>
                                    <div class="col-3 average"><span class="calc_val">1070</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">5350</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Unfixed Swimming Pool or Spa</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">10000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Video Cameras</div>
                                    <div class="col-3 average"><span class="calc_val">900</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">2000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Miscellaneous</div>
                                    <div class="col-3 average"><span class="calc_val">0</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">0</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                            </div>
                        </div>
                        <div class="l-t-f">
                            <div class="row">
                                <div class="col-6 col-lg-3">Total General/personal effects</div>
                                <div class="col-3 t-average general-a">$33695</div>
                                <div class="col-3 t-a-average general-a-a">$100745</div>
                                <div class="col-6 col-lg-3 t-y-v general-v">$0</div>
                            </div>
                        </div>
                    </div>
                    <div class="m-l-tabs-pn"></div>
                  </div>
                  <div id="garage">
                    <div class="l-t">
                        <div class="l-t-h">
                            <div class="row">
                                <div class="col-12">Garage/workshop</div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-lg-3"><span class="d-t">Garage/workshop</span><span class="m-t">Item</span></div>
                                <div class="col-3">Average</div>
                                <div class="col-3">Above Average</div>
                                <div class="col-6 col-lg-3">Your value</div>
                            </div>
                        </div>
                        <div class="l-t-b">
                            <div class="l-t-b-i">
                                <div class="row">
                                    <div class="col-6 col-lg-3">Camping Equipment</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">10000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Garden Hose and Reel</div>
                                    <div class="col-3 average"><span class="calc_val">215</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">535</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Ladders / Step Ladder</div>
                                    <div class="col-3 average"><span class="calc_val">105</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">525</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Lawn Mower / Wheel Barrow</div>
                                    <div class="col-3 average"><span class="calc_val">840</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3000</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Outdoor Furniture</div>
                                    <div class="col-3 average"><span class="calc_val">1100</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">4500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Tools / Power Tools</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">4200</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Miscellaneous</div>
                                    <div class="col-3 average"><span class="calc_val">0</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">0</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number"></div>
                                </div>
                            </div>
                        </div>
                        <div class="l-t-f">
                            <div class="row">
                                <div class="col-6 col-lg-3">Total Garage/workshop</div>
                                <div class="col-3 t-average garage-a">$33695</div>
                                <div class="col-3 t-a-average garage-a-a">$100745</div>
                                <div class="col-6 col-lg-3 t-y-v garage-v">$0</div>
                            </div>
                        </div>
                    </div>
                    <div class="m-l-tabs-pn"></div>
                  </div>
                  <div id="contenttotal">
                    <div class="l-t">
                        <div class="l-t-h">
                            <div class="row">
                                <div class="col-12">Contents total</div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-lg-3"><span class="d-t">Contents total</span><span class="m-t">Item</span></div>
                                <div class="col-3">Average</div>
                                <div class="col-3">Above Average</div>
                                <div class="col-6 col-lg-3">Your value</div>
                            </div>
                        </div>
                        <div class="l-t-b">
                            <div class="l-t-b-i">
                                <div class="row">
                                    <div class="col-6 col-lg-3">Living areas</div>
                                    <div class="col-3 average"><span class="calc_val">2000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">4500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number" disabled /></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Kitchen</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number" disabled /></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bathroom</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number" disabled /></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Laundry</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number" disabled /></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Office</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number" disabled /></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Master bedroom</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number" disabled /></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bedroom 2</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number" disabled /></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Bedroom 3</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number" disabled /></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">General/personal effects</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number" disabled /></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-lg-3">Garage/workshop</div>
                                    <div class="col-3 average"><span class="calc_val">1000</span></div>
                                    <div class="col-3 a-average"><span class="calc_val">3500</span></div>
                                    <div class="col-6 col-lg-3 y-v"><input type="number" disabled /></div>
                                </div>
                            </div>
                        </div>
                        <div class="l-t-f">
                            <div class="row">
                                <div class="col-6 col-lg-3">Contents total</div>
                                <div class="col-3 t-average">$33695</div>
                                <div class="col-3 t-a-average">$100745</div>
                                <div class="col-6 col-lg-3 t-y-v">$0</div>
                            </div>
                        </div>
                        <div class="c-t-f button-white">
                            <div>Wow! I bet you didn&#39;t know your contents were worth that much - what a nightmare trying to replace everything. Haven can help take the stress out of replacing lost, damaged or stolen items should the worse happen, just get in touch.</div>
                        </div>
                    </div>
                    <div class="m-l-tabs-pn"></div>
                </div>
            </div>
        </div>
    ';
    return $text;
}

add_shortcode('haven_borrowing_left', 'potential_borrowing');

function potential_borrowing() {
    $text="
        <div id='borrowingLeft'>
            <div>
                I am looking <select><option value='0'>with someone else</option></select>
            </div>
            <div>
                My annual income <select><option value='0'>after tax</option></select> is <input type='number' value='$8,888.88' />
            </div>
            <div>
                I have <select><option value='0'>0</option></select> dependant children under 18
            </div>
            <div>
                I pay <input type='number' value='$8,888.88' /> in school and childcare costs
            </div>
            <div>
                I have <select><option value='0'>0</option></select> vehicles
            </div>
            <div class='button-blue'>
                <a href='#'>Calculate</a>
            </div>
        </div>
    ";
    return $text;
}

add_shortcode('haven_borrowing_right', 'potential_borrowing_amount');

function potential_borrowing_amount() {
    $text='
        <div id="borrowingRight">
            <div class="mortgage-top">
                <h2>Your results</h2>
                <div class="row">
                    <span class="mortgage-label">Your potential borrowing amount</span>
                    <div class="col-md-6">$<span class="borrowing-amount range-slide">0</span></div>
                    <div class="col-md-6"><input class="borrowing-amount range-slide" type="range" value="0" step="0.01" max="100000" /></div>
                </div>
                <div class="row">
                    <span class="mortgage-label">For a term of</span>
                    <div class="col-md-6"><span class="year-term range-slide">1</span> years</div>
                    <div class="col-md-6"><input class="year-term range-slide" type="range" value="1" min="1" max="30" step="1" /></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <span class="mortgage-label">Interest Rate</span>
                        <select><option>4.19% 1 year fixed</option></select>
                    </div>
                    <div class="col-md-6">
                        <span class="mortgage-label">Frequency</span>
                        <select><option>Monthly</option></select>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="mortgage-bottom">
                <div class="row">
                    <h3>Your repayment amount</h3>
                    <div class="repayment-amount">$<span class="range-slide">0</span></div>
                    <input type="range" class="range-slide" value="0" step="0.01" max="100000" />
                    <span>By increasing your repayments you can decrease your term</span>
                </div>
                <div class="button-white">
                    <a href="#">Apply now</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    ';
    return $text;
}

add_shortcode('mortgage_calc', 'mortgage_calc');

function mortgage_calc() {
    $text='
        <div class="mort-wrap">
            <div class="mortgage-top">
                <div class="row">
                    <div class="col-md-4">
                        <form class="calculator_form">
                            <div class="row">
                                <div class="col-12">
                                     <span class="mortgage-label">loan amount</span><span class="tooltip-icon"></span><div class="tooltip-text"><a href="#!" class="close-tooltip"><img src="/app/themes/BBTFramework/images/closetool.png"></a>The amount you’re considering borrowing, or the outstanding balance on your current mortgage.</div>
                                    <span class="dollar-sign">$</span><input class="borrowing-amount range-slide" type="text" value="" data-value="0" maxlength="10" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                     <span class="mortgage-label">annual Interest Rate</span>
                                    <input class="interest-rate range-slide" type="text" value="5.50" maxlength="4" step="0" /><span class="extra-sign">% p.a</span>
                                </div>
                                <p class="interest-text">See <a href="https://www.interest.co.nz/" target="_blank">interest.co.nz</a> for current mortgage rates.</p>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <span class="mortgage-label">Term length</span><span class="tooltip-icon"></span><div class="tooltip-text"><a href="#!" class="close-tooltip"><img src="/app/themes/BBTFramework/images/closetool.png"></a>The date the mortgage needs to be paid by.</div>
                                    <input class="year-term range-slide" type="text" value="30" maxlength="2" step="1" /><span class="extra-sign">years</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <span class="mortgage-label">Repayment Amount</span><span class="tooltip-icon"></span><div class="tooltip-text"><a href="#!" class="close-tooltip"><img src="/app/themes/BBTFramework/images/closetool.png"></a>Enter your current repayment, or try another amount to see the difference it makes.</div>
                                    <span class="dollar-sign">$</span><input class="monthly-fee range-slide" type="text" value="" maxlength="7" />
                                    <span class="helper_label"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <span class="mortgage-label">Frequency</span><span class="tooltip-icon"></span><div class="tooltip-text"><a href="#!" class="close-tooltip"><img src="/app/themes/BBTFramework/images/closetool.png"></a>Whether you pay your loan off weekly, fortnightly or monthly.</div>
                                    <select class="frequency-option">
                                        <option value="52">Weekly</option>
                                        <option value="12" selected="selected">Monthly</option>
                                        <option value="26">Fortnightly</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <span class="mortgage-label">Extra Repayment</span><span class="tooltip-icon"></span><div class="tooltip-text"><a href="#!" class="close-tooltip"><img src="/app/themes/BBTFramework/images/closetool.png"></a>An extra amount you pay in addition to your regular repayments. This will be calculated as if you made this payment today.</div>
                                    <span class="dollar-sign">$</span><input class="repayment-amount range-slide" type="text" value="" step="0.01" maxlength="7" />
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="mortgage-calculate-button">Calculate</div>
                            <div class="result-calculate button-white-black">
                                <a href="#MortCalc1" class="disable" >Calculate</a>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-8">
                        <div class="mortgage-bottom">
                            <div class="result-wrapper">
                                <h3>Your results</h3>
                                <div class="row">
                                    <div class="col-12">
                                        <h4>How much you\'ll pay</h4>
                                    </div>
                                    <div class="col-xl-7">
                                        <div class="row">
                                            <div class="col-7 right-label repayments_label">Monthly Repayments</div>
                                            <div class="col-5 right-value"><span class="range-slide weeklyrepayments"></span></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 right-label">Interest</div>
                                            <div class="col-6 right-value"><span class="range-slide totalinterest"></span></div>
                                        </div>
                                        <div class="final-results">
                                        <div class="row">
                                            <div class="col-6 right-label">Total to pay</div>
                                            <div class="col-6 right-value bolded"><span class="range-slide totalamountpayment"></span></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 right-label">Total interest saved</div>
                                            <div class="col-6 right-value bolded"><span class="range-slide principalrepaidtotal"></span></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 right-label">Duration</div>
                                            <div class="col-6 right-value"><span class="range-slide duration"></span></div>
                                        </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-xl-5 saved-multiple">
                                        <span class="saved-text">You\'ll pay</span>
                                        <span class="saved-value loantimesvalue">-</span>
                                        <span class="saved-text">The amount you borrow</span>
                                    </div>
                                </div>
                                <div class="row toptip">
                                    <div class="col-2 lamp"></div>
                                    <div class="col-10">
                                        <h5>TOP TIP</h5>
                                        <p>Making just the minimum payment allows a mortgage to run on for its entire term. Increase repayments to save interest and be mortgage free sooner.</p>
                                    </div>
                                </div>
                                <div class="row wrapper-button">
                                    <div class="button-white"><a href="/mortgages/application-form/">Apply for a mortgage</a></div>
                                    <div class="button-white"><a href="#MortCalc1" class="back-button">Back</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    ';
    return $text;
}

 

add_action( 'vc_before_init', 'your_name_integrateWithVC' );

function your_name_integrateWithVC() {
   vc_map( array(
      "name" => "Bar tag test",
      "base" => "bartag",
      "class" => "",
      "category" => "Content",
      "params" => array(
         array(
            "type" => "dropdown",
            "holder" => "div",
            "class" => "",
            "heading" => "Text",
            "param_name" => "foo",
            "value" => "Default param value",
            "description" => "Description for foo param."
         )
      )
   ) );
}

add_shortcode( 'bartagtest', 'bartag_func' );

function bartag_func( $atts, $content = null ) { // New function parameter $content is added!
   extract( shortcode_atts( array(
      'foo' => 'something',
      'color' => '#FFF'
   ), $atts ) );
  
   $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
  
   return "<div style='color:{$color};' data-foo='${foo}'>{$content}</div>";
}

add_shortcode('job_grid', 'job_grid');

function job_grid(){

    $return = '';
    $args = array( 'post_type' => 'job', 'posts_per_page' => -1 );
    $loop = new WP_Query( $args );
    $x = 1;
    while ( $loop->have_posts() ) : $loop->the_post();
        $now    = time();
        $date   = get_field('date_closes');
        $dd = '';
        if($date) {
            $date   = strtotime($date);
            $class  = ($date > $now) ? 'job-available' : 'job-expired';
            
            $dd = '<span class="job-status '.$class.'">Closes: '.date("m-d-Y", $date).'</span>';
        }        
        $return .= '
            <div class="vc_grid-item vc_clearfix  job-box vc_col-sm-6 vc_col-md-4 ">
                <h4><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>
                <div class="job-location">'.get_field('location').'</div>
                <div class="post-blurb">        
                    <p>'.get_the_excerpt().'</p>
                </div>
                <div class="job-type">'.get_field('work_type').'</div>
                '.$dd.'

                <div class="button-white-black">
                    <a href="'.get_the_permalink().'">Read more</a>
                </div>  
                <div class="vc_clearfix"></div>
            </div>
        ';

        if($x == 3){
            $return .= '<div class="job-separator"></div>';
            $x =0;
        }
        $x++;
    endwhile;
    $return = '<div class="haven-container job-rows">'.$return.'</div>';
    return $return;
}

/** MEGA MENU **/
// This is the CodyHouse navigation for the clothing tab, it is overkill for the site.
function html5blank_mega()
{
    wp_nav_menu(
    array(
        'theme_location'  => 'header-nav',
        'menu'            => 'mega-menu-mobile',
        'container'       => 'nav',
        'container_class' => 'cd-dropdown',
        'container_id'    => '',
        'menu_class'      => 'cd-nav',
        'menu_id'         => 'cd-primary-nav',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul class="cd-dropdown-content">%3$s</ul>',
        'depth'           => 4,
        'walker'          => new html5blank_walker_nav_menu()
        )
    );
}

class html5blank_walker_nav_menu extends Walker_Nav_Menu {

// add classes to ul sub-menus
function start_lvl( &$output, $depth = 0, $args = array() ) {
    // depth dependent classes
    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
    $display_depth = ( $depth + 1); // because it counts the first submenu as 0
    $classes = array(
        'sub-menu is-hidden',
        ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
        ( $display_depth >=2 ? 'sub-sub-menu is-hidden' : '' ),
        ( $display_depth <2 ? 'cd-secondary-nav' : '' ),
        'menu-depth-' . $display_depth
        );
    $class_names = implode( ' ', $classes );
    $output .= "\n" . $indent . '<ul class="' . $class_names . '"><li class="go-back"><a href="#0">Back</a>'. "\n";

}

// add main/sub classes to li's and links
 function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 )  {
    global $wp_query;
    $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

    // depth dependent classes
    $depth_classes = array(
        ( $depth == 0 ? '' : 'sub-menu-item' ),
        ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
        ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
        'menu-item-depth-' . $depth
    );
    $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

    // passed classes
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

    

    if (strpos($class_names, 'menu-item-has-children') !== false) {
        $class_names .= " has-children";
    }

    // build html
    $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

    // link attributes
    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
    $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

    $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
        $args->before,
        $attributes,
        $args->link_before,
        apply_filters( 'the_title', $item->title, $item->ID ),
        $args->link_after,
        $args->after
    );

    // build html
    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }
}

add_action('wp_enqueue_scripts', 'bbt_custom_career', 5);

function bbt_custom_career() {
    wp_enqueue_style( 'career-style', get_stylesheet_directory_uri() . '/css/career.css');
    wp_enqueue_script( 'career-script', get_stylesheet_directory_uri() . '/js/career.js');
    if (is_page('application-form') || is_page('thank-you')) {
        wp_enqueue_script( 'application-script', get_stylesheet_directory_uri() . '/js/application-form.js');
        wp_enqueue_style( 'application-style', get_stylesheet_directory_uri() . '/css/application-form.css');

    }
}

/**
 * Conditionally Override Yoast SEO Breadcrumb Trail
 * http://plugins.svn.wordpress.org/wordpress-seo/trunk/frontend/class-breadcrumbs.php
 * -----------------------------------------------------------------------------------
 */

add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail' );

function wpse_100012_override_yoast_breadcrumb_trail( $links ) {
    global $post;

    if ( is_home() || is_singular( 'post' ) || is_archive() ) {
        $breadcrumb[] = array(
            'url' => get_permalink( get_option( 'page_for_posts' ) ),
            'text' => 'Blog',
        );

        // array_splice( $links, 1, -2, $breadcrumb );
    } else if ( is_home() || is_singular( 'job' ) || is_archive() ) {
        $breadcrumb[] = array(
            'url' => get_permalink( get_page_by_path( 'careers' ) ),
            'text' => 'Careers',
        );

        array_splice( $links, 1, -2, $breadcrumb );
    }

    return $links;
}

//add_filter("gform_confirmation_anchor", create_function("","return 0;"));

add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Search', 'theme-slug' ),
        'id' => 'search-header',
        'description' => __( 'Widgets in this area will be shown on search click.', 'theme-slug' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}

if( function_exists('acf_add_options_page') ) {    
    acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}

// remove version from head
remove_action('wp_head', 'wp_generator');

// remove version from scripts and styles
function shapeSpace_remove_version_scripts_styles($src) {
    if (strpos($src, 'ver=')) {
        $src = remove_query_arg('ver', $src);
    }
    return $src;
}
add_filter('style_loader_src', 'shapeSpace_remove_version_scripts_styles', 9999);
add_filter('script_loader_src', 'shapeSpace_remove_version_scripts_styles', 9999);


function testimonial_shortcode( $atts ) {

    ob_start();

    get_template_part('includes/testimonials');

    return ob_get_clean();
}

add_shortcode('testimonials', 'testimonial_shortcode');

?>